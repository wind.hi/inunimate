<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.partials.head')
</head>

<body id="page-top">
<div id="wrapper">
    <!-- Sidebar -->
    @include('admin.partials.sidebar')
    <!-- Sidebar -->

    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('admin.partials.nav')
            <!-- MAIN -->
            @yield('content')
            <!-- End MAIN -->
        </div>
        <!-- Footer -->
        @include('admin.partials.footer')
        <!-- Footer -->
    </div>

</div>

<!-- Scroll to top -->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('admin/js/ruang-admin.min.js')}}"></script>
<script src="{{asset('admin/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('admin/js/demo/chart-area-demo.js')}}"></script>

@stack('scripts')
</body>

</html>
