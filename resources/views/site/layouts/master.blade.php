<!DOCTYPE html>
<html lang="en">

<head>
    @include('site.partials.head')
</head>

<body class="barber_version">

<!-- HEADER -->
@include('site.partials.header')
<!-- End HEADER -->

<!-- MAIN -->
@yield('content')
<!-- End MAIN -->

<!-- FOOTER -->
@include('site.partials.footer')
<!-- End FOOTER -->

</body>
<!-- IMPORTED JS -->
@include('site.partials.bottom_js')
</html>
