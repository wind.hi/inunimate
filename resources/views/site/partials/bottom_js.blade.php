<!-- ALL JS FILES -->
<script src="{{asset('js/all.js')}}"></script>
<script src="{{asset('js/responsive-tabs.js')}}"></script>
<!-- ALL PLUGINS -->
<script src="{{asset('js/custom.js')}}"></script>
@stack('scripts')
