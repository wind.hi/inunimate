<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-about-us">
                    <h2><span>Tien Minh</span></h2>
                    <p>Tiến Minh - Chuyên cung cấp sỉ lẻ thùng xốp các loại</p>
                    <p> Giá cả cạnh tranh - chất lượng uy tín</p>
                    <div class="footer-social">
                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">HƯỚNG DẪN</h2>
                    <ul>
                        <li><a href="#">Báo giá & hỗ trợ</a></li>
                        <li><a href="#">Về chúng tôi</a></li>
                        <li><a href="#">Liên hệ</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">Danh mục</h2>
                    <ul>
                        <li><a href="#">Thùng xốp</a></li>
                        <li><a href="#"></a></li>

                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-newsletter">
                    <h2 class="footer-wid-title">Bản tin</h2>
                    <p>Để lại email để nhận được thông báo về sản phẩm, khuyễn mãi mới nhất từ TienMinh</p>
                    <div class="newsletter-form">
                        <form action="#">
                            <input type="email" placeholder="nhập email của bạn">
                            <input type="submit" value="Đăng ký">
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p> © Feb - 2021 thungxoptienminh.net
                    </p>
                </div>
            </div>

            <div class="col-md-4">
            </div>
        </div>
    </div>
</div> <!-- End footer bottom area -->

<!-- Latest jQuery form server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="{{asset('site/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('site/js/jquery.sticky.js')}}"></script>

<!-- jQuery easing -->
<script src="{{asset('site/js/jquery.easing.1.3.min.js')}}"></script>

<!-- Main Script -->
<script src="{{asset('site/js/main.js')}}"></script>

<!-- Slider -->
<script type="text/javascript" src="{{asset('site/js/bxslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/js/script.slider.js')}}"></script>

@stack('scripts')
