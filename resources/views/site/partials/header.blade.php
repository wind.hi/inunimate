<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="./"><img src="{{asset('site/img/logo.png')}}"></a></h1>
                </div>
            </div>

{{--            <div class="col-sm-6">--}}
{{--                <div class="shopping-item">--}}
{{--                    <a href="cart.html">Cart - <span class="cart-amunt">$100</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</div> <!-- End site branding area -->

<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{route('products.index')}}">Trang chủ</a></li>
                    <?php
                    $categories = \Modules\Category\Entities\Category::query()->where('parent', 0)->get();
                    ?>
                    @foreach($categories as $category)
                    <li><a>{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div> <!-- End mainmenu area -->
