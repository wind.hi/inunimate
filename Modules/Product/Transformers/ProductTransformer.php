<?php
namespace Modules\Product\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Product\Entities\Product;

class ProductTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'created_at' => $product->created_at,
            'updated_at' => $product->updated_at,
        ];
    }
}