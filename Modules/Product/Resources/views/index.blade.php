@extends('admin.layouts.master')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">DataTables</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active" aria-current="page">DataTables</li>
            </ol>
        </div>

        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <table class="table align-items-center table-flush" id="dataTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Mã code</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Đơn vị tính</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Mã code</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Đơn vị tính</th>
                                <th>Thao tác</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($products as $product)
                                <tr id="{{$product->id}}">
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->code}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->total}}</td>
                                    <td>{{$product->unit}}</td>
                                    <td>
                                        <a href="{{route('admin.products.show', $product->id)}}"
                                           class="btn btn-info btn-sm"
                                           title="chi tiết">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                        @if($product->active == \Modules\Product\Entities\Product::HIDE)
                                            <a class="btn btn-default btn-sm" title="hiện"
                                               data-product_id="{{$product->id}}" data-action="show"
                                               onclick="return showHideProduct(this);">
                                                <i class="fas fa-eye-slash"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-default btn-sm" title="ẩn"
                                               data-product_id="{{$product->id}}" data-action="hide"
                                               onclick="return showHideProduct(this);">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        @endif
                                        <a class="btn btn-danger btn-sm" onclick="return showModalConfirm(this);"
                                           data-product_id="{{$product->id}}">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!--Row-->

        <!-- Documentation Link -->
        <div class="row">
            <div class="col-lg-12">
                <p>DataTables is a third party plugin that is used to generate the demo table below. For more
                    information
                    about DataTables, please visit the official <a href="https://datatables.net/" target="_blank">DataTables
                        documentation.</a></p>
            </div>
        </div>

        <!-- Modal Logout -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Đồng ý xóa sản phẩm này?</p>
                        <input type="hidden" id="product-id-hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Đóng</button>
                        <a class="btn btn-primary" onclick="return deleteProduct(this);">Đồng ý</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!---Container Fluid-->

@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/toast/jquery.toast.min.js')}}" charset="utf-8"></script>

    <!-- Page level custom scripts -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable(); // ID From dataTable
            $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        });
    </script>

    <script>
        function showHideProduct(e) {
            let productId = $(e).data('product_id');
            let action = $(e).data('action');
            $.ajax({
                url: '{{route('ajax.products.update.active')}}',
                method: 'GET',
                data: {
                    product_id: productId,
                    action: action,
                },
                success: function (data) {
                    if (action == 'hide') {
                        $(e).attr('title', 'hiện');
                        $(e).data('action', 'show');
                        $(e).find($('i')).removeClass('fas fa-eye').addClass('fas fa-eye-slash');
                    } else {
                        $(e).attr('title', 'ẩn');
                        $(e).data('action', 'hide');
                        $(e).find($('i')).removeClass('fas fa-eye-slash').addClass('fas fa-eye');
                    }
                },
                error: function (error) {
                    console.log(error);
                }

            });
        }

        function showModalConfirm(e) {
            $("#logoutModal").modal('show');
            $("input#product-id-hidden").val($(e).data('product_id'));
        }

        function deleteProduct(e) {
            let productId = $("input#product-id-hidden").val();
            $.ajax({
                url: '{{route('ajax.products.delete')}}',
                method: 'DELETE',
                data: {
                    _token: '{{csrf_token()}}',
                    product_id: productId
                },
                success: function (data) {
                    $("tr#" + productId).remove();
                    $("#logoutModal").modal('hide');

                    $.toast({
                        position: 'top-right',
                        text: 'Đã xóa sản phẩm',
                        icon: 'success',
                    });

                },
                error: function (error) {
                    console.log(error);
                    $.toast({
                        position: 'top-right',
                        text: 'Có lỗi xảy ra !!!',
                        icon: 'error',
                    });
                }

            });

        }
    </script>
@endpush
