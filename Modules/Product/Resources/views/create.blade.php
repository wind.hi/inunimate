@extends('admin.layouts.master')
@section('content')
        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="./">Home</a></li>
                    <li class="breadcrumb-item">Forms</li>
                    <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
                </ol>
            </div>
            @if(session()->has('messageSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('messageSuccess') }}
                </div>
            @endif
            <div class="row">

                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thêm sản phẩm</h6>
                        </div>
                        <form method="post" action="{{route('admin.products.store')}}" enctype="multipart/form-data"
                              id="add-product">
                            @csrf
                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="card-body">
                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Ảnh sản phẩm</label>
                                            <input type="file" accept="image/*" name='file' onchange="loadFile(event)">


                                        </div>
                                        <img id="output" width="180px" height="180px"
                                             src="{{asset('storage/file_system/empty.jpg')}}">
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="card-body">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tên sản phẩm</label>
                                            <input type="text" class="form-control" id=""
                                                   name="name" value="{{old('name')}}"
                                                   placeholder="nhập tên sản phẩm">
                                        </div>
                                        @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mã sản phẩm</label>
                                            <input type="text" class="form-control" id=""
                                                   name="code" value="{{old('code')}}"
                                                   placeholder="nhập mã sản phẩm">
                                        </div>
                                        @error('code')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Nhà cung cấp</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="vendor_id">
                                                <option value selected>-- chọn nhà cung cấp --</option>
                                                @foreach($vendors as $vendor)
                                                    <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Giá tiền</label>
                                            <input type="text" class="form-control formatPrice" id=""
                                                   name="price" value="{{old('price')}}"
                                                   placeholder="nhập giá tiền">
                                        </div>
                                        @error('price')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Giảm Giá</label>
                                            <span class="help">( % )</span>
                                                <input type="number" class="form-control" min="0" name="sale" value="{{old('sale') ?? 0}}">
                                        </div>
                                        @error('sale')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Số lượng</label>
                                            <input type="text" class="form-control" id=""
                                                   name="total" value="{{old('total')}}"
                                                   placeholder="nhập giá tiền">
                                        </div>
                                        @error('total')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Đơn vị tính</label>
                                            <input type="text" class="form-control" id=""
                                                   name="unit" value="{{old('unit')}}"
                                                   placeholder="nhập đơn vị">
                                        </div>
                                        @error('unit')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Danh mục</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="category">
                                                <option value selected>-- chọn danh mục --</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        @error('category')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Nhập giá trị thuộc tính</label>
                                            <div class="table-responsive">

                                            </div>
                                            <input type="hidden" name="attributes">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        </div>

                                        <button type="button" class="btn btn-primary" id="btn-add-product">Thêm mới
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>

        </div>
        <!---Container Fluid-->
@endsection

@push('scripts')
    <script src="{{asset('admin/toast/jquery.toast.min.js')}}" charset="utf-8"></script>
    <script src="{{ asset('admin/js/jquery.priceformat.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
        })
    </script>
    <script>
        $("select[name='category']").change(function () {
            let categoryId = $(this).val();

            if (categoryId === '') {
                $("div.table-responsive").empty();
                return;
            }

            $.ajax({
                    url: '{{route('ajax.products.get.attribute')}}',
                    method: 'GET',
                    data: {
                        category_id: categoryId
                    },
                    success: function (data) {
                        $("div.table-responsive").html(data);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                },
            );

        })

        $("button#btn-add-product").click(function () {
            let attributes = [];

            $('tr.attributes').each(function (i, obj) {

                let attId = $(obj).find('input').data('att_id');
                let attType = $(obj).find('input').data('att_type');
                let value = $(obj).find('input').val();

                attributes.push({'id': attId, 'type': attType, 'value': value});
            });

            var myJSON = JSON.stringify(attributes);

            $('input:hidden[name=attributes]').val(myJSON);

            var frm = $('form#add-product');
            var form = $('form#add-product')[0];
            var formData = new FormData(form);

            $.ajax({
                    method: frm.attr('method'),
                    url: frm.attr('action'),
                    data: formData,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $.toast({
                            heading: 'Thêm thành công sản phẩm !',
                            position: 'top-right',
                            text: 'Sản phẩm đã được thêm mới. Quay lại danh sách sản phẩm để xem chi tiết.',
                            icon: 'success',
                            // afterHidden: function () {
                            //     location.reload();
                            // }
                        });
                    },
                    error: function (request, status, error) {
                        let message = jQuery.parseJSON( request.responseText );
                        let messageErrors = message.errors;
                        // console.log(message.errors.name);
                        for (var key of Object.keys(messageErrors)) {
                            var messArr =  messageErrors[key];
                            var arrayLength = messArr.length;
                            for (var i = 0; i < arrayLength; i++) {
                                $.toast({
                                    text: (messArr[i]),
                                    position: 'top-right',
                                    icon: 'error'
                                });
                            }
                            // console.log(message[key]);
                        }

                        // alert ('Lỗi xảy ra: '+JSON.stringify(message.errors));
                    }
                },
            );

        });

        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endpush
