@extends('site.layouts.master')
@section('content')
    <style>
        label.error {
            color: firebrick;
            font-size: 13px;
        }
        button[type=submit],  button[type=button] {
            background: none repeat scroll 0 0 #5a88ca;
            border: medium none;
            color: #fff;
            padding: 11px 20px;
            text-transform: uppercase;
        }
    </style>
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2 style="font-size: 35px">Chi tiết sản phẩm</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Tìm kiếm</h2>
                        <input type="text" placeholder="nhập tên, mã sản phẩm..." name="keyword">
                        <button type="button" id="search">Tìm kiếm</button>
                    </div>

                    {{--                    <div class="single-sidebar">--}}
                    {{--                        <h2 class="sidebar-title">Products</h2>--}}
                    {{--                        <div class="thubmnail-recent">--}}
                    {{--                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">--}}
                    {{--                            <h2><a href="">Sony Smart TV - 2015</a></h2>--}}
                    {{--                            <div class="product-sidebar-price">--}}
                    {{--                                <ins>$700.00</ins>--}}
                    {{--                                <del>$100.00</del>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="thubmnail-recent">--}}
                    {{--                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">--}}
                    {{--                            <h2><a href="">Sony Smart TV - 2015</a></h2>--}}
                    {{--                            <div class="product-sidebar-price">--}}
                    {{--                                <ins>$700.00</ins>--}}
                    {{--                                <del>$100.00</del>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="thubmnail-recent">--}}
                    {{--                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">--}}
                    {{--                            <h2><a href="">Sony Smart TV - 2015</a></h2>--}}
                    {{--                            <div class="product-sidebar-price">--}}
                    {{--                                <ins>$700.00</ins>--}}
                    {{--                                <del>$100.00</del>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="thubmnail-recent">--}}
                    {{--                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">--}}
                    {{--                            <h2><a href="">Sony Smart TV - 2015</a></h2>--}}
                    {{--                            <div class="product-sidebar-price">--}}
                    {{--                                <ins>$700.00</ins>--}}
                    {{--                                <del>$100.00</del>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="single-sidebar">--}}
                    {{--                        <h2 class="sidebar-title">Recent Posts</h2>--}}
                    {{--                        <ul>--}}
                    {{--                            <li><a href="">Sony Smart TV - 2015</a></li>--}}
                    {{--                            <li><a href="">Sony Smart TV - 2015</a></li>--}}
                    {{--                            <li><a href="">Sony Smart TV - 2015</a></li>--}}
                    {{--                            <li><a href="">Sony Smart TV - 2015</a></li>--}}
                    {{--                            <li><a href="">Sony Smart TV - 2015</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </div>--}}
                </div>

                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="product-breadcroumb">
                            <a href="">Trang chủ</a>
                            <?php
                            $categories = $product->category;
                            $cateArr = [];
                            $cateIdArr = [];
                            $parentId = [];

                            foreach ($categories as $category) {
                                $cateArr[] = $category->name;
                                $parentId[] = $category->parent;
                                $cateIdArr[] = $category->id;
                            }

                            $parentCategory = \Modules\Category\Entities\Category::query()->whereIn('id', $parentId)->first();

                            $stringCategory = implode(',', $cateArr);
                            ?>

                            @if(!empty($parentCategory))
                                <a href="">{{$parentCategory->name}}</a>
                            @endif

                            <a href="">{{$stringCategory}}</a>
                            <a href="">{{$product->name}}</a>

                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="product-images">
                                    <?php
                                    if (isset($product->file)) {
                                        $fileImage = $product->file->path;
                                    } else {
                                        $fileImage = 'file_system/empty.jpg';
                                    }
                                    ?>

                                    <div class="product-main-img">
                                        <img src="{{asset('storage/'.$fileImage)}}" alt=""
                                             style="width: 260px; height: auto">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="product-inner">
                                    <h2 class="product-name">{{$product->name}}</h2>

                                    <div class="product-inner-price">
                                        @if($product->price !== null)
                                            Giá:
                                            <ins>{{number_format($product->price)}} vnđ</ins>
                                        @else
                                            Giá:
                                            <ins>Liên hệ</ins>
                                        @endif
                                    </div>
                                    <div class="product-inner-price">
                                        Số lượng trong kho: <strong>{{$product->total}} ({{$product->unit}})</strong>
                                    </div>
                                    {{--                                    <form action="" class="cart">--}}
                                    {{--                                        <div class="quantity">--}}
                                    {{--                                            <input type="number" size="4" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <button class="add_to_cart_button" type="submit">Add to cart</button>--}}
                                    {{--                                    </form>--}}

                                    <div class="product-inner-category">
                                        <p>Danh mục: <a href="">{{$stringCategory}}</a></p>
                                    </div>

                                    <div role="tabpanel">
                                        <ul class="product-tab" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                                      role="tab" data-toggle="tab">Sản
                                                    phẩm</a>
                                            </li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile"
                                                                       role="tab" data-toggle="tab">Đặt hàng</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                                <h2>Thông tin chi tiết</h2>
                                                @foreach($product->productAttributeValue as $productAttributeValue)
                                                    <?php
                                                    $relationAttributeType = 'attributeValue' . ucwords($productAttributeValue->attribute->type);
                                                    ?>
                                                    <p>{{$productAttributeValue->attribute->name}}
                                                        : {{$productAttributeValue->$relationAttributeType->value}}</p>
                                                @endforeach
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                                <h2>Liện hệ đặt hàng</h2>
                                                <form action="{{route('orders.store')}}" method="POST" id="frm-orders">
                                                    @csrf
                                                    <div class="submit-review">

                                                        <p><label for="customer_name">Họ tên</label>
                                                            <input class="form-control height-control"
                                                                   name="customer_name" type="text" id="customer_name"
                                                                   value="{{old('customer_name')}}">
                                                            @if ($errors->has('customer_name'))
                                                                <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('customer_name') }}
                                                                    </span>
                                                            @endif
                                                        </p>
                                                        <input type="hidden" name="product_id"
                                                               value="{{$product->id}}">
                                                        <p><label for="customer_phone">Số điện thoại</label>
                                                            <input
                                                                class="form-control height-control"
                                                                name="customer_phone" type="text"
                                                                value="{{old('customer_phone')}}">
                                                        </p>
                                                        @if ($errors->has('customer_phone'))
                                                            <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('customer_phone') }}
                                                            </span>
                                                        @endif
                                                        <p><label for="customer_email">Email</label>
                                                            <input class="form-control height-control"
                                                                   name="customer_email" type="email"
                                                                   value="{{old('customer_email')}}"></p>
                                                        @if ($errors->has('customer_email'))
                                                            <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('customer_email') }}
                                                            </span>
                                                        @endif
                                                        <p><label for="customer_address">Địa chỉ</label> <input
                                                                class="form-control height-control"
                                                                name="customer_address" type="text"
                                                                value="{{old('customer_address')}}"></p>
                                                        @if ($errors->has('customer_address'))
                                                            <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('customer_address') }}
                                                            </span>
                                                        @endif
                                                        <p><label for="quantity">Số lượng mua ({{$product->unit}}
                                                                )</label> <input name="quantity" type="text" size="5"
                                                                                 class="form-control height-control"
                                                                                 value="{{old('quantity')}}"></p>
                                                        @if ($errors->has('quantity'))
                                                            <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('quantity') }}
                                                            </span>
                                                        @endif
                                                        <p><label for="note">Lời nhắn</label> <textarea
                                                                class="form-control height-control"
                                                                name="note"></textarea></p>
                                                        @if ($errors->has('note'))
                                                            <span class="help-block" style="color:red">
                                                                 * {{ $errors->first('note') }}
                                                            </span>
                                                        @endif
                                                        <p>
                                                            <button type="submit" id="btn-submit">Đặt hàng</button>
                                                        </p>
                                                        {{--                                                        <p style="display: flex; margin-left:10px; margin-top: 5px; text-align: justify;font-size: 14px; color: midnightblue" id="please-wait">Vui lòng giữ màn hình 1-2 phút tùy điều kiện mạng. Chúng tôi đang xử lý đơn hàng. Cảm ơn sự kiên nhẫn của quý khách.</p>--}}

                                                    </div>
                                                </form>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php
                        $productsRelate = \Modules\Product\Entities\Product::query()->whereHas('category', function (\Illuminate\Database\Eloquent\Builder $query) use ($cateIdArr, $product) {
                            $query->whereIn('category_id', $cateIdArr)->whereNotIn('product_id', [$product->id]);
                        })->get();
                        ?>
                        <div class="related-products-wrapper">
                            <h2 class="related-products-title">Sản phẩm liên quan</h2>
                            <div class="related-products-carousel">
                                @foreach($productsRelate as $productRelate)
                                    <div class="single-product">
                                        <div class="product-f-image">
                                            <?php
                                            if (isset($productRelate->file)) {
                                                $fileImage = $productRelate->file->path;
                                            } else {
                                                $fileImage = 'file_system/empty.jpg';
                                            }
                                            ?>
                                            <img src="{{asset('storage/'.$fileImage)}}" alt="">
                                            <div class="product-hover">
                                                <a href="{{route('products.show', $productRelate->slug)}}"
                                                   class="view-details-link"><i class="fa fa-link"></i> chi tiết</a>
                                            </div>
                                        </div>

                                        <h2>
                                            <a href="{{route('products.show', $productRelate->slug)}}">{{$productRelate->name}}</a>
                                        </h2>

                                        <div class="product-carousel-price">
                                            @if($productRelate->price !== null)
                                                Giá:
                                                <ins>{{number_format($productRelate->price)}} vnđ</ins>
                                            @else
                                                Giá:
                                                <ins>Liên hệ</ins>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{asset('site/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <script>
        // $('#frm-orders').submit(function(event){
        //     let x = document.getElementById('frm-orders').checkValidity();
        //     if (x) {
        //         $('#btn-submit').attr('disabled','true');
        //         $('#btn-submit').text('Đang xử lý ...');
        //         $("p#please-wait").css( "visibility", "visible");
        //         $("#frm-orders").submit();
        //         $('#btn-submit').removeAttribute('disabled');
        //     }
        // });

        $.validator.addMethod("validatePhone", function (value, element) {
                return this.optional(element) || /(84|0|\+84)[0-9]{9}$/i.test(value);
            }
        );

        $.validator.addMethod("validateQuantity", function (value, element) {
                return checkValue(value);
            }
        );

        function checkValue(value) {
            let quantity = '{{$product->total}}';
            if (parseInt(value) > quantity) {
                return false;
            } else {
                return true;
            }
        }

        $(document).ready(function () {
            // $("p#please-wait").css('visibility', 'hidden');
            $("#frm-orders").validate({
                rules: {
                    customer_name: "required",
                    customer_address: "required",
                    "quantity": {
                        required: true,
                        digits: true,
                        validateQuantity: true
                    },
                    "customer_email": {
                        email: true
                    },
                    "customer_phone": {
                        required: true,
                        validatePhone: true
                    },
                },
                messages: {
                    customer_name: "* Vui lòng nhập họ tên",
                    customer_address: "* Vui lòng nhập địa chỉ",
                    quantity: {
                        required: "* Vui lòng nhập số lượng cần đặt",
                        digits: "* Số lượng đặt chỉ chấp nhận kiểu số nguyên",
                        validateQuantity: "* Vui lòng nhập số lượng thấp hơn số lượng trong kho"
                    },
                    "customer_email": {
                        email: "* Vui lòng đúng định dang email"
                    },
                    "customer_phone": {
                        required: "* Vui lòng nhập số điện thoại",
                        validatePhone: "* Vui lòng nhập đúng định dạng điện thoại. ví dụ: 0363676992"
                    },
                },

            });
        });

        $('button#search').click(function () {
            let keyword = $("input[name='keyword']").val();

            if (keyword.replace(/^\s+|\s+$/g, "").length != 0) {
                let url = '{{route('products.search')}}' + '?keyword=' + keyword;
                window.location.href = url;
            } else {
                return;
            }

        })
    </script>
@endpush
