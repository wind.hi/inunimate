@extends('site.layouts.master')
@section('content')
    <div class="jumbotron text-center">
        <h1 class="display-3">Cảm ơn bạn đã đặt hàng !</h1>
        <p class="lead">Vui lòng kiểm tra lại đơn hàng qua email của bạn. Chúng tôi sẽ sớm liên lạc lại với bạn !</p>
        <hr>
        <p>
            Có vấn đề ? <a>Liên hệ với chúng tôi</a>
        </p>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="{{route('products.index')}}" role="button">Quay lại trang chủ</a>
        </p>
    </div>
@endsection
