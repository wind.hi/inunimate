@extends('site.layouts.master')
@section('content')
    <div class="slider-area">
        <!-- Slider -->
        <div class="block-slider block-slider4">
            <ul class="" id="bxslider-home4">
                <li>
                    <img src="{{asset('site/img/h4-slide.png')}}" alt="Slide">
                    <div class="caption-group">
                        <h2 class="caption title">
                            iPhone <span class="primary">6 <strong>Plus</strong></span>
                        </h2>
                        <h4 class="caption subtitle">Dual SIM</h4>
                        <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a>
                    </div>
                </li>
                <li><img src="{{asset('site/img/h4-slide2.png')}}" alt="Slide">
                    <div class="caption-group">
                        <h2 class="caption title">
                            by one, get one <span class="primary">50% <strong>off</strong></span>
                        </h2>
                        <h4 class="caption subtitle">school supplies & backpacks.*</h4>
                        <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a>
                    </div>
                </li>
                <li><img src="{{asset('site/img/h4-slide3.png')}}" alt="Slide">
                    <div class="caption-group">
                        <h2 class="caption title">
                            Apple <span class="primary">Store <strong>Ipod</strong></span>
                        </h2>
                        <h4 class="caption subtitle">Select Item</h4>
                        <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a>
                    </div>
                </li>
                <li><img src="{{asset('site/img/h4-slide4.png')}}" alt="Slide">
                    <div class="caption-group">
                        <h2 class="caption title">
                            Apple <span class="primary">Store <strong>Ipod</strong></span>
                        </h2>
                        <h4 class="caption subtitle">& Phone</h4>
                        <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- ./Slider -->
    </div> <!-- End slider area -->

    <div class="promo-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo1">
                        <i class="fa fa-refresh"></i>
                        <p>30 ngày đổi trả</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo2">
                        <i class="fa fa-truck"></i>
                        <p>Giao hàng miễn phí</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo3">
                        <i class="fa fa-lock"></i>
                        <p>Thông tin bảo mật</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo4">
                        <i class="fa fa-gift"></i>
                        <p>Uy tín - Chất lượng</p>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End promo area -->

    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="section-title">Sản phẩm mới nhất</h2>
                        <div class="product-carousel">
                            @foreach($products as $product)
                                <div class="single-product">
                                    <div class="product-f-image">
                                        <?php
                                        if (isset($product->file)) {
                                            $fileImage = $product->file->path;
                                        } else {
                                            $fileImage = 'file_system/empty.jpg';
                                        }
                                        ?>
                                        <img src="{{asset('storage/'.$fileImage)}}" alt="">

                                        <div class="product-hover">
                                            <a href="{{route('products.show', $product->slug)}}"
                                               class="view-details-link"><i class="fa fa-link"></i> Chi tiết</a>
                                        </div>

                                    </div>

                                    <h2><a href="{{route('products.show', $product->slug)}}">{{$product->name}}</a></h2>

                                    <div class="product-carousel-price">
                                        @if($product->price !== null)
                                            <ins>{{number_format($product->price)}} vnđ</ins>
                                        @else
                                            <ins>Liên hệ</ins>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->

    {{--    <div class="brands-area">--}}
    {{--        <div class="zigzag-bottom"></div>--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-md-12">--}}
    {{--                    <div class="brand-wrapper">--}}
    {{--                        <div class="brand-list">--}}
    {{--                            <img src="img/brand1.png" alt="">--}}
    {{--                            <img src="img/brand2.png" alt="">--}}
    {{--                            <img src="img/brand3.png" alt="">--}}
    {{--                            <img src="img/brand4.png" alt="">--}}
    {{--                            <img src="img/brand5.png" alt="">--}}
    {{--                            <img src="img/brand6.png" alt="">--}}
    {{--                            <img src="img/brand1.png" alt="">--}}
    {{--                            <img src="img/brand2.png" alt="">--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div> <!-- End brands area -->--}}

    <div class="product-widget-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-4">
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Mua nhiều nhất</h2>
                        <a href="" class="wid-view-more">Xem thêm</a>

                        @foreach($productBestSells as $productBestSell)
                            <div class="single-wid-product">
                                <a href="{{route('products.show', $productBestSell->slug)}}">
                                    <?php
                                    if (isset($productBestSell->file)) {
                                        $fileImage = $productBestSell->file->path;
                                    } else {
                                        $fileImage = 'file_system/empty.jpg';
                                    }
                                    ?>
                                    <img src="{{asset('storage/'.$fileImage)}}" class="product-thumb">

                                </a>
                                <h2>
                                    <a href="{{route('products.show', $productBestSell->slug)}}">{{$productBestSell->name}}</a>
                                </h2>

                                <div class="product-wid-price">
                                    @if($productBestSell->price !== null)
                                        <ins>{{number_format($productBestSell->price)}} vnđ</ins>
                                    @else
                                        <ins>Liên hệ</ins>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Đang khuyến mãi</h2>
                        <a href="#" class="wid-view-more">Xem thêm</a>
                        @foreach($productSales as $productSale)
                            <div class="single-wid-product">
                                <a href="{{route('products.show', $productSale->slug)}}">
                                    <?php
                                    if (isset($productSale->file)) {
                                        $fileImage = $productSale->file->path;
                                    } else {
                                        $fileImage = 'file_system/empty.jpg';
                                    }
                                    ?>
                                    <img src="{{asset('storage/'.$fileImage)}}" class="product-thumb">

                                </a>
                                <h2><a href="{{route('products.show', $productSale->slug)}}">{{$productSale->name}}</a>
                                </h2>

                                <div class="product-wid-price">
                                    @if($productSale->price !== null)
                                        <ins>{{number_format($productSale->price_promotion)}} vnđ</ins>
                                        <del>{{number_format($productSale->price)}} vnđ</del>
                                    @else
                                        <ins>Liên hệ</ins>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-2"></div>

            </div>
        </div>
    </div> <!-- End product widget area -->
@endsection
