@extends('site.layouts.master')
@section('content')
        <div class="product-big-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-bit-title text-center">
                            <h2 style="font-size: 30px">Tìm thấy {{$products->count()}} kết quả phù hợp với từ khóa '{{$keyword}}'</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-product-area">
            <div class="zigzag-bottom"></div>
            <div class="container">
                <div class="row">
                    @foreach($products->paginate(8) as $product)
                    <div class="col-md-3 col-sm-6">
                        <div class="single-shop-product">
                            <div class="product-upper">
                                <?php
                                if(isset($product->file)) {
                                    $fileImage = $product->file->path;
                                } else{
                                    $fileImage = 'file_system/empty.jpg';
                                }
                                ?>
                                <img src="{{asset('storage/'.$fileImage)}}" alt="" >
                            </div>
                            <h2><a href="{{route('products.show', $product->slug)}}">{{$product->name}}</a></h2>
                            <div class="product-carousel-price">
                                @if($product->price !== null)
                                    Giá: <ins>{{number_format($product->price)}} vnđ</ins>
                                @else
                                    Giá:  <ins>Liên hệ</ins>
                                @endif
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="product-pagination text-center">
                            {{ $products->paginate(8)->links() }}
{{--                            <nav>--}}
{{--                                <ul class="pagination">--}}
{{--                                    <li>--}}
{{--                                        <a href="#" aria-label="Previous">--}}
{{--                                            <span aria-hidden="true">&laquo;</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li><a href="#">1</a></li>--}}
{{--                                    <li><a href="#">2</a></li>--}}
{{--                                    <li><a href="#">3</a></li>--}}
{{--                                    <li><a href="#">4</a></li>--}}
{{--                                    <li><a href="#">5</a></li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" aria-label="Next">--}}
{{--                                            <span aria-hidden="true">&raquo;</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </nav>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>

    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- jQuery sticky menu -->
    <script src="{{asset('site/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('site/js/jquery.sticky.js')}}"></script>

    <!-- jQuery easing -->
    <script src="{{asset('site/js/jquery.easing.1.3.min.js')}}"></script>

    <!-- Main Script -->
    <script src="{{asset('site/js/main.js')}}s"></script>
    <script>

    </script>
@endpush
