<table class="table align-items-center table-flush">
    <thead class="thead-light">
    <tr>
        <th>Thuộc tính</th>
        <th>Kiểu</th>
        <th>Nhập giá trị</th>
    </tr>
    </thead>
    <tbody>
    @foreach($attributes as $attribute)
        <tr id="{{$attribute->id}}" class="attributes">
            <td>{{$attribute->name}}</td>
            <td>{{$attribute->type}}</td>
                <?php
                    if($attribute->type == 'date') {
                        $typeInput = 'date';
                    }else{
                        $typeInput = 'text';
                    }
                ?>
            <td><input type="{{$typeInput}}" data-att_type="{{$attribute->type}}" data-att_id="{{$attribute->id}}" name="att_value" class="form-control"></td>
        </tr>
    @endforeach

    </tbody>
</table>
