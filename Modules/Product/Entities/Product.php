<?php

namespace Modules\Product\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Attribute\Entities\Attribute;
use Modules\Category\Entities\Category;
use Modules\File\Entities\Files;
use Modules\Order\Entities\Order;
use Modules\Vendor\Entities\Vendor;

/**
 * Class Product
 * @property int $id
 * @package Modules\Product\Entities
 *
 * @property string $name
 * @property string $code
 * @property double $price
 * @property int $sale
 * @property int $total
 * @property string $unit
 * @property int $vendor_id
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * Relations
 * @property-read Collection|Attribute[]  $attributeValueInt
 * @property-read Collection|Attribute[]  $attributeValueString
 * @property-read Collection|Attribute[]  $attributeValueFloat
 * @property-read Collection|Attribute[]  $attributeValueDate
 * @property-read Collection|Attribute[]  $attributeValueProduct
 * @property-read Collection|ProductAttributeValue[] $productAttributeValue
 * @property-read Collection|Order[] $orders
 * @property-read Category $category
 *
 * Accessors
 * @property-read float|int $price_promotion
 */
class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const SHOW = 1;
    const HIDE = 0;

    protected $fillable = ['name', 'code', 'price', 'total', 'unit','vendor_id', 'active', 'slug', 'sale'];

    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();

        self::deleting(function (self $product) {

            foreach ($product->productAttributeValue as $productAttributeValue) {

                $attributeValueType = ($productAttributeValue->type);

                switch ($attributeValueType) {
                    case 'int':
                        $product->attributeValueInt()->wherePivot('attribute_id', $productAttributeValue->attribute_id)->detach();

                        break;

                    case 'float':
                        $product->attributeValueFloat()->wherePivot('attribute_id', $productAttributeValue->attribute_id)->detach();

                        break;

                    case 'string':
                        $product->attributeValueString()->wherePivot('attribute_id', $productAttributeValue->attribute_id)->detach();

                        break;

                    case 'date':
                        $product->attributeValueDate()->wherePivot('attribute_id', $productAttributeValue->attribute_id)->detach();

                        break;
                }
            }

            $product->productAttributeValue()->delete();
            $product->category()->detach();
            $product->file()->delete();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributeValueInt(){
        return $this->belongsToMany(Attribute::class, 'attribute_value_int', 'product_id', 'attribute_id')->withPivot('id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributeValueString(){
        return $this->belongsToMany(Attribute::class, 'attribute_value_string', 'product_id', 'attribute_id')->withPivot('id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributeValueFloat(){
        return $this->belongsToMany(Attribute::class, 'attribute_value_float', 'product_id', 'attribute_id')->withPivot('id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributeValueDate(){
        return $this->belongsToMany(Attribute::class, 'attribute_value_date', 'product_id', 'attribute_id')->withPivot('id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributeValueProduct(){
        return $this->belongsToMany(Attribute::class, 'product_attribute_values', 'product_id', 'attribute_id')->withPivot('id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|ProductAttributeValue[]
     */
    public function productAttributeValue(){
        return $this->hasMany(ProductAttributeValue::class,'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne|Files
     */
    public function file()
    {
        return $this->morphOne(Files::class, 'fileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Category
     */
    public function category(){
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id')->withTimestamps()->limit(1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Vendor
     */
    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }

    /**
     * @return \Awssat\Visits\Visits
     */
    public function vzt()
    {
        return visits($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Order[]
     */
    public function orders(){
        return $this->hasMany(Order::class,'product_id');
    }

    /**
     * @return float|int
     */
    public function getPricePromotionAttribute(){
        $sale = $this->sale/100;
        $pricePromotion = $this->price * (1-$sale);

        return $pricePromotion;
    }
}
