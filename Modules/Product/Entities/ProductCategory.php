<?php

namespace Modules\Product\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory
 * @property int $id
 * @package Modules\Product\Entities
 *
 * @property int $product_id
 * @property int $category_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * Relations
 *
 */
class ProductCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'product_id', 'category_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'product_categories';

}
