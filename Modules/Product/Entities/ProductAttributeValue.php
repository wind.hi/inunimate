<?php

namespace Modules\Product\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Entities\AttributeValueDate;
use Modules\Attribute\Entities\AttributeValueFloat;
use Modules\Attribute\Entities\AttributeValueInt;
use Modules\Attribute\Entities\AttributeValueString;

/**
 * Class ProductAttributeValue
 * @property int $id
 * @package Modules\Product\Entities
 *
 * @property int $product_id
 * @property int $attribute_id
 * @property int $attribute_value_id
 * @property string $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * Relations
 *
 */
class ProductAttributeValue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'product_id', 'attribute_id', 'attribute_value_id', 'type'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|AttributeValueInt
     */
    public function attributeValueInt(){
        return $this->belongsTo(AttributeValueInt::class, 'attribute_value_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|AttributeValueString
     */
    public function attributeValueString(){
        return $this->belongsTo(AttributeValueString::class, 'attribute_value_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|AttributeValueFloat
     */
    public function attributeValueFloat(){
        return $this->belongsTo(AttributeValueFloat::class, 'attribute_value_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|attributeValueDate
     */
    public function attributeValueDate(){
        return $this->belongsTo(AttributeValueDate::class, 'attribute_value_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|attributeValueDate
     */
    public function attribute(){
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }
}
