<?php

namespace Modules\Product\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Product API Request
 *
 * Class ProductUpdateRequest
 * @package Modules\Api\Http\Requests
 */
class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'nullable|unique:products,code,'.$this->route('id').',id',
            'price' => 'nullable|numeric',
            'total' => 'required|numeric',
            'unit' => 'required',
            'vendor_id' => 'required|exists:vendors,id',
            'file' => 'max:8192|mimes:jpg,png,svg,gif'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm không được để trống',
            'code.unique' => 'Mã sản phẩm đã tồn tại, nhập lại mã khác',
            'total.required' => 'Tổng số sản phẩm không được để trống',
            'total.numeric' => 'Tổng số sản phẩm dạng số',
            'price.numeric' => 'Giá sản phẩm dạng số',
            'unit.required' => 'Đơn vị sản phẩm không được để trống',
            'vendor_id.required' => 'Nhà cung cấp không được để trống',
            'vendor_id.exists' => 'Nhà cung cấp không tồn tại',
            'file.max' => 'Hình ảnh dung lượng nhỏ hơn 8MB',
            'file.mimes' => 'Hình ảnh chỉ chấp nhận định dạng đuôi .jpg, .png, .svg, .gif',
        ];
    }
}
