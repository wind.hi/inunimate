<?php

namespace Modules\Product\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Product API Request
 *
 * Class ProductCreateRequest
 * @package Modules\Api\Http\Requests
 */
class ProductCreateRequest extends FormRequest
{
    public $messages = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $attributes = json_decode($this->get('attributes'));
//        dd($attributes);
//        dd(json_decode($this->get('attributes')));
        $rules = [
            'name' => 'required',
            'code' => 'nullable|unique:products,code',
//            'price' => 'nullable|numeric',
//            'price' => "nullable|regex:/^\d+(\.\d{1,3})?$/",
//            'price' => "nullable|regex:/^(?!^0\.00$)(([1-9][\d]{0,6})|([0]))\.[\d]{2}$/",
            'sale' => 'nullable|integer',
            'total' => 'required|numeric',
            'unit' => 'required',
            'category' => 'required|exists:categories,id',
            'vendor_id' => 'required|exists:vendors,id',
            'file' => 'required|max:8192|mimes:jpg,png,svg,gif'
        ];

        foreach($attributes as $key => $val)
        {

            if(isset($attributes[$key]->type)){
                if($attributes[$key]->type == 'int'){
                    $rules['attributes.'.$key.'.'.'value']         = 'integer';
                    $this->messages['attributes.'.$key.'.'.'value.integer']  = 'Thuộc tính '.$this->input('attributes.'.$key.'.'.'name'). ' là số nguyên và độ dài từ 1 - 20';
                }
                if($attributes[$key]->type == 'float'){
                    $rules['attributes.'.$key.'.'.'value']         = 'numeric';
                    $this->messages['attributes.'.$key.'.'.'value.numeric']  = 'Thuộc tính '.$this->input('attributes.'.$key.'.'.'name'). ' phải là kiểu số';
                }
                if($attributes[$key]->type == 'string'){
                    $rules['attributes.'.$key.'.'.'value']         = 'string|max:255';
                    $this->messages['attributes.'.$key.'.'.'value.string']  = 'Thuộc tính '.$this->input('attributes.'.$key.'.'.'name'). ' chỉ nhận kiểu chuỗi';
                    $this->messages['attributes.'.$key.'.'.'value.max']  = 'Thuộc tính '.$this->input('attributes.'.$key.'.'.'name'). ' tối đa 255 ký tự';
                }
            }
        }
//        dd($rules);
        return $rules;
    }

    public function messages()
    {
        $messages = $this->messages;
        $messages['name.required'] = 'Tên sản phẩm không được để trống';
        $messages['code.unique'] = 'Mã sản phẩm đã tồn tại, nhập lại mã khác';
        $messages['total.required'] =  'Tổng số sản phẩm không được để trống';
        $messages['total.numeric'] = 'Tổng số sản phẩm dạng số';
        $messages['sale.integer'] = 'Khuyến mãi dạng số nguyên';
        $messages['unit.required'] = 'Đơn vị sản phẩm không được để trống';
        $messages['category.required'] = 'Danh mục sản phẩm không được để trống';
        $messages['vendor_id.required'] = 'Nhà cung cấp không được để trống';
        $messages['vendor.exists'] = 'Nhà cung cấp không tồn tại';
        $messages['file.required'] = 'Hình ảnh sản phẩm không được để trống';
        $messages['file.max'] = 'Hình ảnh dung lượng nhỏ hơn 8MB';
        $messages['file.mimes'] = 'Hình ảnh chỉ chấp nhận định dạng đuôi .jpg, .png, .svg, .gif';

        return $messages;
    }
}
