<?php

namespace Modules\Product\Http\Controllers\Site;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\Category;
use Modules\Product\Entities\Product;
use Modules\Product\Repositories\Criteria\ProductCriteriaKeyWord;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Service\ProductService;

class ProductController extends Controller
{
    protected $repository;
    protected $productService;

    public function __construct(ProductRepository $repository, ProductService $productService)
    {
        $this->repository = $repository;
        $this->productService = $productService;
    }

    /** index trang home - lười tạo homecontroller =x
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = $this->repository->orderBy('created_at', 'desc')->findByField('active', Product::SHOW);

        /**
         * @var Category $category
         *  ghim 1 cate ngoài trang chủ để đếm tổng số lượt ghé thăm
         */
        $category = Category::query()->find(1);
        $category->vzt()->increment();

        /** lấy sản phẩm bán được nhiều nhất */
        $productBestSells = $this->repository->withCount('orders')
            ->orderBy('orders_count', 'desc')
            ->take(3)->get();

        /** lấy sản phẩm có khuyến mãi */
        $productSales = $this->repository->where('active', Product::SHOW)->where('sale', '>', 0)->take(3)->get();

        return view('product::site.index', compact('products', 'productBestSells', 'productSales'));
    }

    /**
     * Show the specified resource.
     * @param string $slug
     * @return Renderable
     */
    public function show($slug)
    {
        $product = $this->repository->findByField('slug', $slug)->first();

        return view('product::site.detail', compact('product'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function search(Request $request)
    {
        $keyword = $request->get('keyword');

        $products = $this->repository
            ->pushCriteria(new ProductCriteriaKeyword($keyword))
            ->orderBy('created_at', 'desc');

        return view('product::site.search_result', compact('products', 'keyword'));
    }
}
