<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\Category;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\ProductCreateRequest;
use Modules\Product\Http\Requests\ProductUpdateRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Service\ProductService;
use Modules\Vendor\Entities\Vendor;

class ProductController extends Controller
{
    protected $repository;
    protected $productService;

    public function __construct(ProductRepository $repository, ProductService $productService)
    {
        $this->repository = $repository;
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = $this->repository->orderBy('created_at', 'desc')->get();
        return view('product::index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = Category::query()->get();

        $vendors = Vendor::query()->get();
        return view('product::create', compact('categories', 'vendors'));
    }

    /**
     * @param ProductCreateRequest $request
     */
    public function store(ProductCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['name', 'code', 'price', 'total', 'unit', 'vendor_id', 'sale']);

            $file = $request->file('file');

            $attributes = json_decode($request->input('attributes'));

            $product = $this->productService->create($data, $attributes, $file);

            $product->category()->sync($request->category);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $product = $this->repository->find($id);

        $vendors = Vendor::query()->get();

        return view('product::detail', compact('product', 'vendors'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('product::edit');
    }

    /**
     * @param ProductUpdateRequest $request
     * @param $id
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['name', 'code', 'price', 'total', 'unit', 'vendor_id', 'sale']);

            $attributes = json_decode($request->input('attributes'));

            $file = $request->file('file');

            $this->productService->update($id, $data, $attributes, $file);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
        }

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajaxGetAttribute(Request $request)
    {
        $categoryId = $request->get('category_id');

        $attributes = $this->productService->ajaxGetAttributeByCategoryId($categoryId);

        return view('product::ajax.list_attributes', compact('attributes'));
    }

    /**
     * @param Request $request
     */
    public function ajaxShowHideProduct(Request $request)
    {
        $productId = $request->get('product_id');
        $action = $request->get('action');

        if ($action == 'hide') {
            $active = Product::HIDE;
        } else {
            $active = Product::SHOW;
        }

        /** @var Product $product */
        $product = $this->repository->find($productId);

        $product->active = $active;

        $product->save();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function homePage()
    {
        $totalProduct = Product::query()->count();

        $category = Category::query()->find(1);

        if($category){
            $totalView = $category->vzt()->count();
        }else{
            $totalView = 0;
        }

        $totalOrder = Order::query()->count();

        return view('product::homepage', compact('totalView', 'totalOrder', 'totalProduct'));
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            /** @var Product $product */
            $product = $this->repository->find($request->input('product_id'));

            $product->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * clear all data
     */
    public function clearData(){
        DB::table('products')->truncate();
        DB::table('product_attribute_values')->truncate();
        DB::table('product_categories')->truncate();
        DB::table('attribute_value_date')->truncate();
        DB::table('attribute_value_int')->truncate();
        DB::table('attribute_value_float')->truncate();
        DB::table('attribute_value_string')->truncate();
        DB::table('attributes')->truncate();
        DB::table('categories')->truncate();
        DB::table('attribute_categories')->truncate();
        DB::table('files')->truncate();
        DB::table('orders')->truncate();
        DB::table('vendors')->truncate();
        DB::table('visits')->truncate();
        DB::table('customers')->truncate();
        DB::table('notifications')->truncate();
    }
}
