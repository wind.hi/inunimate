<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::prefix('products')->group(function () {
        Route::get('', 'ProductController@index')->name('admin.products.index');
        Route::get('create', 'ProductController@create')->name('admin.products.create');
        Route::post('', 'ProductController@store')->name('admin.products.store');
        Route::get('{id}', 'ProductController@show')->name('admin.products.show');
        Route::put('{id}', 'ProductController@update')->name('admin.products.update');

        Route::delete('', 'ProductController@delete')->name('ajax.products.delete');

        /** handle ajax */
        Route::get('ajax/get-attribute', 'ProductController@ajaxGetAttribute')->name('ajax.products.get.attribute');

        Route::get('ajax/update-active-product', 'ProductController@ajaxShowHideProduct')->name('ajax.products.update.active');
    });
    /** homepage */
    Route::get('home', 'ProductController@homepage')->name('admin.index');

    /** clear data */
    Route::get('/clear', 'ProductController@clearData')->name('admin.clear.data');
});

Route::group(['namespace' => 'Site'], function () {
    Route::get('', 'ProductController@index')->name('products.index');
    Route::prefix('san-pham')->group(function () {
        Route::get('{slug}', 'ProductController@show')->name('products.show');
    });
    Route::get('/tim-kiem', 'ProductController@search')->name('products.search');
});
