<?php

namespace Modules\Product\Service;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Attribute\Repositories\AttributeRepository;
use Modules\Category\Repositories\CategoryRepository;
use Modules\File\Entities\Files;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Controllers\ProductController;
use Modules\Product\Repositories\ProductRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ProductService
 */
class ProductService
{
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;

        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param int $categoryId
     * @return \Illuminate\Database\Eloquent\Collection|\Modules\Attribute\Entities\Attribute[]
     */
    public function ajaxGetAttributeByCategoryId($categoryId)
    {
        /** @var \Modules\Category\Entities\Category $category */
        $category = $this->categoryRepository->find($categoryId);

        $attributes = $category->attributes;

        return $attributes;
    }

    /**
     * @param $data
     * @param $attributes
     * @param $file
     * @return Product
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create($data, $attributes, $file)
    {
//        $data['slug'] = Str::slug($data['name']) . uniqid();
        $data['slug'] = Str::slug($data['name']) . '-' . mt_rand(10000, 99999) . Product::query()->max('id');
        $data['price'] = str_replace('.', '', $data['price']);
        $data['sale'] = str_replace('.', '', $data['sale']);

        $product = $this->productRepository->create($data);

        if (!empty($attributes)) {
            $syncProductAttributeValue = [];

            foreach ($attributes as $attribute) {
                $attributeValueId = $this->insertAttributeProduct($attribute, $product);

                $syncProductAttributeValue[$attribute->id] = ['attribute_value_id' => $attributeValueId, 'type' => $attribute->type];
            }
            $product->attributeValueProduct()->sync($syncProductAttributeValue);

        }

        if (!empty($file)) {
            $this->uploadFile($file, $product);
        }

        return $product;
    }

    /**
     * @param $id
     * @param $data
     * @param $attributes
     * @param $file
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, $data, $attributes, $file)
    {
        $data['price'] = str_replace('.', '', $data['price']);
        $data['sale'] = str_replace('.', '', $data['sale']);
        $product = $this->productRepository->update($data, $id);

        if (!empty($file)) {
            Files::query()->where([
                'fileable_id' => $product->id,
                'fileable_type' => Product::class
            ])->delete();

            $this->uploadFile($file, $product);
        }

        if (!empty($attributes)) {
            $syncProductAttributeValue = [];

            foreach ($attributes as $attribute) {

                $attributeValueId = $this->insertAttributeProduct($attribute, $product);

                $syncProductAttributeValue[$attribute->id] = ['attribute_value_id' => $attributeValueId, 'type' => $attribute->type];
            }
//            $product->attributeValueProduct()->sync($syncProductAttributeValue);
        }
    }

    /**
     * @param $attribute
     * @param $product
     * @return array
     */
    private function insertAttributeProduct($attribute, $product)
    {
        $type = $attribute->type;
        $attributeValueId = '';
        switch ($type) {
            case 'int':
                $product->attributeValueInt()->syncWithoutDetaching([$attribute->id => ['value' => $attribute->value]]);

                $attributeValueId = $product->attributeValueInt()->wherePivot('product_id', $product->id)
                    ->wherePivot('attribute_id', $attribute->id)->first()->pivot->id;
                break;

            case 'float':
                $product->attributeValueFloat()->syncWithoutDetaching([$attribute->id => ['value' => $attribute->value]]);

                $attributeValueId = $product->attributeValueFloat()->wherePivot('product_id', $product->id)
                    ->wherePivot('attribute_id', $attribute->id)->first()->pivot->id;
                break;

            case 'string':
                $product->attributeValueString()->syncWithoutDetaching([$attribute->id => ['value' => $attribute->value]]);

                $attributeValueId = $product->attributeValueString()->wherePivot('product_id', $product->id)
                    ->wherePivot('attribute_id', $attribute->id)->first()->pivot->id;
                break;

            case 'date':
                $product->attributeValueDate()->syncWithoutDetaching([$attribute->id => ['value' => Carbon::parse($attribute->value)->format('Y-m-d')]]);

                $attributeValueId = $product->attributeValueDate()->wherePivot('product_id', $product->id)
                    ->wherePivot('attribute_id', $attribute->id)->first()->pivot->id;
                break;
        }

//        $result = [$attribute->id => ['attribute_value_id' => $attributeValueId, 'type' => $type]];
//        return $result;

        return $attributeValueId;

    }

    /**
     * @param UploadedFile $file
     * @param Product $product
     */
    public function uploadFile($file, $product)
    {
        $file_name = uniqid() . '-' . $file->getClientOriginalName();

        /**
         * TODO : valid phần thêm thuộc tính product
         * TODO : đổi mật khẩu
         * TODO : thông báo khi có đơn hàng trên phần header admin
         */
        $photo = Image::make($file)
            ->fit(195, 243, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('jpg', 90);

        Storage::disk('public')->put('products/' . $file_name, $photo);
//        Storage::disk('public')->put('products/' . $file_name, file_get_contents($file));

        $fileData['name'] = $file_name;
        $fileData['path'] = 'products/' . $file_name;;
        $fileData['path_thumbnail'] = '';
        $fileData['fileable_id'] = $product->id;
        $fileData['fileable_type'] = Product::class;

        $image = new Files($fileData);
        $image->save();
    }

    /**
     * @param integer $productId
     * @param integer $action
     */
    public function showHideProduct($productId, $action)
    {
        $product = $this->productRepository->find($productId);
        $product->active = $action;
        $product->save();
    }
}
