<?php

return [
    'name' => 'Product',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'product.browse',
        'product.create',
        'product.update',
        'product.destroy'
    ]
];