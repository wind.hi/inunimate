<?php

namespace Modules\Product\Repositories;

use Modules\Product\Entities\Product;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ProductRepository
 * @package Modules\Platform\User\Repositories
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return Product
     */
    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return Product
     */
    public function update(array $attributes, $id)
    {
        return parent::update($attributes, $id);
    }

    public function ajaxGetAttributes($categoryId){

    }
}
