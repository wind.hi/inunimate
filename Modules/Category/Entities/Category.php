<?php

namespace Modules\Category\Entities;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Attribute\Entities\Attribute;

/**
 * Class Category
 * @property int $id
 * @package Modules\Category\Entities
 *
 * @property string $name
 * @property string $slug
 *
 * Relations
 * @property-read Collection|Attribute[] $attributes
 */
class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'slug', 'parent'];

    protected $table = 'categories';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Attribute[]
     */
    public function attributes(){
        return $this->belongsToMany(Attribute::class, 'attribute_categories', 'category_id', 'attribute_id')->withTimestamps();
    }

    /**
     * @return \Awssat\Visits\Visits
     */
    public function vzt()
    {
        return visits($this);
    }
}
