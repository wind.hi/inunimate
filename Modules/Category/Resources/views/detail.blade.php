@extends('admin.layouts.master')
@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Cập nhật danh mục</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Forms</li>
                <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
            </ol>
        </div>
        @if(session()->has('messageSuccess'))
            <div class="alert alert-success">
                {{ session()->get('messageSuccess') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-6">
                <!-- Form Basic -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Cập nhật danh mục</h6>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.categories.update', $category->id)}}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên danh mục</label>
                                <input type="text" class="form-control" id=""
                                       name="name" value="{{$category->name}}"
                                       placeholder="nhập tên danh mục">
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <?php
                            $arrName = [];
                            foreach ($categoryChildren as $categoryChild) {
                                $arrName[] = $categoryChild->name;
                            }
                            $stringCateChild = implode(', ', $arrName);
                            ?>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Danh mục con: {{$stringCateChild}}</label>

                            </div>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
            </div>
        </div>


        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-8 mb-4">
                <!-- Simple Tables -->
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Thuộc tính</h6>

                        <button class="btn btn-success btn-sm" onclick="return addAttribute(this);">
                            <i class="fa fa-plus"></i>
                        </button>

                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="tbl-attribute">
                            <thead class="thead-light">
                            <tr>
                                <th>Tên</th>
                                <th>Kiểu giá trị</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attributeCategory as $attribute)
                                <tr>
                                    <td>{{$attribute->name}}</td>
                                    <td>{{$attribute->type}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>

    </div>
    <!---Container Fluid-->
@endsection
@push('scripts')
    <script src="<?php echo e(asset('admin/toast/jquery.toast.min.js')); ?>" charset="utf-8"></script>
    <script>
        function addAttribute(e) {
            $('table#tbl-attribute').find($('tbody')).append('<tr><td><input class="form-control" type="text" name="name_att"></td>' +
                '<td><select class="form-control" name="type_att">' +
                '<option value="int">số nguyên</option> ' +
                '<option value="float">số thập phân</option> ' +
                '<option value="string">chuỗi</option> ' +
                '<option value="date">ngày tháng</option> <td><button class="btn btn-success mb-1" onclick="return saveAttribute(this)">thêm</button></td>' +
                '</select></td><tr/>')
        }

        function saveAttribute(e) {
            let attributeName = $(e).closest($('tr')).find($('input[name="name_att"]')).val();
            let attributeType = $(e).closest($('tr')).find($('select[name="type_att"] option:selected')).val();
            console.log(attributeType);
            $.ajax({
                    url: '{{route('admin.categories.add.attribute')}}',
                    method: "POST",
                    data: {
                        _token: '{{csrf_token()}}',
                        attribute_name: attributeName,
                        attribute_type: attributeType,
                        category_id: '{{$category->id}}',
                    },
                    success: function (data) {
                        $(e).closest($('tr')).empty();
                        $('table#tbl-attribute').find($('tbody')).append('<tr><td>'+attributeName+'</td>' +
                            '<td> '+attributeType+'</td>');
                        $.toast({
                            text: 'Thêm thuộc tính vào danh mục thành công!.',
                            icon: 'success',
                        });
                    },
                    error: function (error) {
                        console.log(error);
                    }
                },
            );

        }
    </script>
@endpush
