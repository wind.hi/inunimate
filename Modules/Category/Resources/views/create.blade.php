@extends('admin.layouts.master')
@section('content')

        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Form Basics</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="./">Home</a></li>
                    <li class="breadcrumb-item">Forms</li>
                    <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
                </ol>
            </div>
            @if(session()->has('messageSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('messageSuccess') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-6">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thêm danh mục</h6>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{route('admin.categories.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên danh mục</label>
                                    <input type="text" class="form-control" id=""
                                           name="name" value="{{old('name')}}"
                                           >
                                </div>
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                 @enderror

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Danh mục cha</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="category_parent">
                                        <option value selected>-- chọn danh mục --</option>
                                        @foreach($categoryParents as $categoryParent)
                                            <option value="{{$categoryParent->id}}">{{$categoryParent->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2">
                </div>

            </div>

        </div>
        <!---Container Fluid-->
@endsection
