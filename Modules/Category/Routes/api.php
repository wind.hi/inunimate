<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'CategoryController@index');
        Route::get('{id}', 'CategoryController@show');
        Route::post('', 'CategoryController@store');
        Route::put('{id}', 'CategoryController@update');
    });
});

