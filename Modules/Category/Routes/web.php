<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** route group admin */
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::prefix('categories')->group(function () {

        Route::get('create/', 'CategoryController@create')->name('admin.categories.create');
        Route::get('', 'CategoryController@index')->name('admin.categories.index');
        Route::get('{id}', 'CategoryController@show')->name('admin.categories.show');

        Route::put('{id}', 'CategoryController@update')->name('admin.categories.update');
        Route::post('', 'CategoryController@store')->name('admin.categories.store');

        Route::post('add-attribute', 'CategoryController@addAttribute')->name('admin.categories.add.attribute');

    });

});
