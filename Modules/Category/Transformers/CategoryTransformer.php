<?php
namespace Modules\Category\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Category\Entities\Category;

class CategoryTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'created_at' => $category->created_at,
            'updated_at' => $category->updated_at,
        ];
    }
}