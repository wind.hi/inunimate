<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\Attribute\Service\AttributeService;
use Modules\Category\Entities\Category;
use Modules\Category\Http\Requests\CategoryCreateRequest;
use Modules\Category\Http\Requests\CategoryUpdateRequest;
use Modules\Category\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    protected $repository;
    protected $attributeService;

    public function __construct(CategoryRepository $repository, AttributeService $attributeService)
    {
        $this->repository = $repository;
        $this->attributeService = $attributeService;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $categories = $this->repository->orderBy('created_at', 'desc')->findByField('parent', 0);
        return view('category::index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categoryParents = $this->repository->findByField('parent',0);
        return view('category::create', compact('categoryParents'));
    }

    /**
     * @param CategoryCreateRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(CategoryCreateRequest $request)
    {
        try {
            $data = ['name' => $request->name, 'parent' => $request->category_parent, 'slug' => Str::slug($request->name)];

            $category = $this->repository->create($data);

            return redirect()->back()->with('messageSuccess', 'Thêm mới danh mục thành công!');
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        /** @var Category $category */
        $category = $this->repository->find($id);

        $categoryChildren = $this->repository->where('parent', $category->id)->get();

        $attributeCategory = $category->attributes;

        return view('category::detail', compact('category', 'attributeCategory', 'categoryChildren'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('category::edit');
    }

    /**
     * @param CategoryUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        try {
            $data = $request->only('name');

            $category = $this->repository->update($data, $id);

            return redirect()->back()->with('messageSuccess', 'Cập nhật danh mục thành công!');
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function addAttribute(Request $request){
        $data = ['name' => $request->input('attribute_name'), 'type' => $request->input('attribute_type')];

        $category = [$request->input('category_id')];

        $this->attributeService->create($data, $category);
    }
}
