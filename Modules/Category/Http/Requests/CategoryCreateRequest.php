<?php

namespace Modules\Category\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Category API Request
 *
 * Class CategoryCreateRequest
 * @package Modules\Api\Http\Requests
 *
 * @property $name
 */
class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:categories'
        ];
    }
}
