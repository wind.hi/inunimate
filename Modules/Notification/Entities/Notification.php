<?php

namespace Modules\Notification\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notifications
 * @package Modules\Notifications\Entities
 *
 * @property int $id
 * @property string $type
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property string $data
 * @property Carbon $read_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Notifications extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','type', 'notifiable_type', 'notifiable_id', 'data'];

    /**
     * @var string
     */
    protected $table = 'notifications';

    protected $dates = ['read_at', 'created_at', 'updated_at'];

}
