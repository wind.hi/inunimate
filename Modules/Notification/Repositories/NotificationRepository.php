<?php

namespace Modules\Notification\Repositories;

use Modules\Notification\Entities\Notification;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class NotificationRepository
 * @package Modules\Platform\User\Repositories
 */
class NotificationRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return Notification
     */
    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return Notification
     */
    public function update(array $attributes, $id)
    {
        return parent::update($attributes, $id);
    }
}