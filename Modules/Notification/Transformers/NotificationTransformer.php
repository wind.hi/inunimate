<?php
namespace Modules\Notification\Transformers;

use Carbon\Carbon;
use Illuminate\Notifications\DatabaseNotification;
use League\Fractal\TransformerAbstract;
use Modules\Notification\Entities\Notifications;

class NotificationTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Notifications $notification)
    {
        return [
            'id' => $notification->id,
            'type' => $notification->type,
            'data' => $notification->data,
            'notifiable_type' => $notification->notifiable_type,
            'notifiable_id' => $notification->notifiable_id,
            'read_at' => $notification->read_at->format('Y-m-d H:i:s'),
            'created_at' => $notification->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $notification->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
