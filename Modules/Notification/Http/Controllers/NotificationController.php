<?php

namespace Modules\Notification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Routing\Controller;
use Modules\Notification\Entities\Notifications;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user = auth('admin')->user();

        /** @var DatabaseNotification $notification */
        $notifications = $user->notifications();


        $notifications = $notifications->paginate(intval($request->get('per_page')));

        $notifications->getCollection()->transform(function (DatabaseNotification $notification) {
            $data['id'] = $notification->id;
            $data['data'] = $notification->data;
            $data['type'] = $notification->type;
            $data['notifiable_type'] = $notification->notifiable_type;
            $data['notifiable_id'] = $notification->notifiable_id;
            $data['read_at'] = $notification->read_at;
            $data['created_at'] = $notification->created_at;
            $data['updated_at'] = $notification->updated_at;
            return new Notifications($data);
        });



        return $this->responseSuccess($this->transform($notifications, NotificationTransformer::class, $request));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('notification::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('notification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('notification::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
