<?php

namespace Modules\Notification\Http\Requests;
use App\Http\Requests\ApiBaseRequest;

/**
 * Notification API Request
 *
 * Class NotificationCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class NotificationUpdateApiRequest extends ApiBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
