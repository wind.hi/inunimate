<?php

return [
    'name' => 'Notification',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'notification.browse',
        'notification.create',
        'notification.update',
        'notification.destroy'
    ]
];