<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

    Route::prefix('vendors')->group(function() {
        Route::get('', 'VendorController@index')->name('admin.vendors.index');
        Route::get('create', 'VendorController@create')->name('admin.vendors.create');
        Route::post('', 'VendorController@store')->name('admin.vendors.stores');
        Route::get('{id}', 'VendorController@show')->name('admin.vendors.show');
        Route::put('{id}', 'VendorController@update')->name('admin.vendors.update');
    });

});
