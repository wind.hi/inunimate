<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'vendor'], function () {
        Route::get('', 'VendorController@index');
        Route::get('{id}', 'VendorController@show');
        Route::post('', 'VendorController@store');
        Route::put('{id}', 'VendorController@update');
    });
});

