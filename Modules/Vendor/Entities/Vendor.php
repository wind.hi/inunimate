<?php

namespace Modules\Vendor\Entities;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Vendor
 * @property int $id
 * @property string $name
 * @property string $code
 * @package Modules\Vendor\Entities
 */
class Vendor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'code'];

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'vendors';
}
