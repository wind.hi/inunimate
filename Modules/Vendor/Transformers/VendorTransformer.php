<?php
namespace Modules\Vendor\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Vendor\Entities\Vendor;

class VendorTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Vendor $vendor)
    {
        return [
            'id' => $vendor->id,
            'created_at' => $vendor->created_at,
            'updated_at' => $vendor->updated_at,
        ];
    }
}