@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Forms</li>
                <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
            </ol>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <!-- Form Basic -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Cập nhật nhà cung cấp</h6>
                    </div>
                    <form method="post" action="{{route('admin.vendors.update', $vendor->id)}}" enctype="multipart/form-data"
                          id="add-product">
                        @method('PUT')
                        @csrf
                        <div class="row">

                            <div class="col-lg-2">

                            </div>

                            <div class="col-lg-8">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã nhà cung cấp</label>
                                        <input type="text" class="form-control" id=""
                                               name="code" value="{{$vendor->code}}"
                                               placeholder="nhập mã nhà cung cấp">
                                    </div>
                                    @error('code')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tên nhà cung cấp</label>
                                        <input type="text" class="form-control" id=""
                                               name="name" value="{{$vendor->name}}"
                                               placeholder="nhập tên sản phẩm">
                                    </div>
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    <button type="submit" class="btn btn-primary" id="btn-add-product">Lưu</button>
                                </div>
                            </div>

                            <div class="col-lg-2">

                            </div>



                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>
    <!---Container Fluid-->
@endsection

@push('scripts')


@endpush
