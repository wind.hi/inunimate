<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Vendor\Http\Requests\VendorCreateRequest;
use Modules\Vendor\Http\Requests\VendorUpdateRequest;
use Modules\Vendor\Repositories\VendorRepository;

class VendorController extends Controller
{
    /**
     * @var VendorRepository
     */
    protected $repository;

    public function __construct(VendorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $vendors = $this->repository->orderBy('created_at', 'desc')->get();
        return view('vendor::index', compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('vendor::create');
    }

    /**
     * @param VendorCreateRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(VendorCreateRequest $request)
    {
        try {
            $data = $request->only(['name', 'code']);

            $vendor = $this->repository->create($data);

            return redirect()->back()->with('success', 'Thêm mới nhà cung cấp thành công !');
        } catch (\Exception $exception) {
            return response()->json([
                "error" => $exception->getMessage()
            ], 500);
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $vendor = $this->repository->find($id);
        return view('vendor::detail', compact('vendor'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('vendor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param VendorUpdateRequest $request
     * @param int $id
     * @return Renderable
     */
    public function update(VendorUpdateRequest $request, $id)
    {
        try {
            $data = $request->only(['name', 'code']);

            $vendor = $this->repository->update($data, $id);

            return redirect()->back()->with('success', 'Cập nhật nhà cung cấp thành công !');
        } catch (\Exception $exception) {
            return response()->json([
                "error" => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
