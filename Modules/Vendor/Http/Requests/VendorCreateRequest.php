<?php

namespace Modules\Vendor\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Vendor API Request
 *
 * Class VendorCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class VendorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required|unique:vendors',
        ];
    }
}
