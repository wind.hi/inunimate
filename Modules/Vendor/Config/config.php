<?php

return [
    'name' => 'Vendor',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'vendor.browse',
        'vendor.create',
        'vendor.update',
        'vendor.destroy'
    ]
];