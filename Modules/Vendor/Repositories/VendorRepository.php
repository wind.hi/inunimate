<?php

namespace Modules\Vendor\Repositories;

use Modules\Vendor\Entities\Vendor;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class VendorRepository
 * @package Modules\Platform\User\Repositories
 */
class VendorRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Vendor::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return Vendor
     */
    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return Vendor
     */
    public function update(array $attributes, $id)
    {
        return parent::update($attributes, $id);
    }
}