@extends('admin.layouts.master')
@section('content')
        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Form Basics</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="./">Home</a></li>
                    <li class="breadcrumb-item">Forms</li>
                    <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
                </ol>
            </div>
            @if(session()->has('messageSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('messageSuccess') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-6">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thêm mới thuộc tính</h6>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{route('admin.attributes.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên thuộc tính</label>
                                    <input type="text" class="form-control" id=""
                                           name="name" value="{{old('name')}}"
                                           placeholder="nhập tên thuộc tính">
                                </div>
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Kiểu thuộc tính</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="type">
                                        <option value selected>-- chọn kiểu thuộc tính --</option>
                                        <option value="int">số nguyên</option>
                                        <option value="float">số thập phân</option>
                                        <option value="string">chuỗi</option>
                                        <option value="date">ngày tháng</option>
                                    </select>
                                </div>
                                @error('type')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                                <div class="form-group">
                                    <label for="select2Multiple">Danh mục</label>
                                    <select class="select2-multiple form-control" name="category[]" multiple="multiple"
                                            id="select2Multiple">
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('category')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2">
                </div>

            </div>

        </div>
        <!---Container Fluid-->
@endsection

@push('scripts')
    <!-- Select2 -->
    <script src="{{asset('admin/vendor/select2/dist/js/select2.min.js')}}"></script>
    <!-- Bootstrap Datepicker -->
    <script src="{{asset('admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- Bootstrap Touchspin -->
    <script src="{{asset('admin/vendor/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js')}}"></script>
    <!-- ClockPicker -->
    <script src="{{asset('admin/vendor/clock-picker/clockpicker.js')}}"></script>
    <!-- Javascript for this page -->
    <script>
        $(document).ready(function () {


            $('.select2-single').select2();

            // Select2 Single  with Placeholder
            $('.select2-single-placeholder').select2({
                placeholder: "Select a Province",
                allowClear: true
            });

            // Select2 Multiple
            $('.select2-multiple').select2();

            // Bootstrap Date Picker
            $('#simple-date1 .input-group.date').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: 'linked',
                todayHighlight: true,
                autoclose: true,
            });

            $('#simple-date2 .input-group.date').datepicker({
                startView: 1,
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true,
                todayBtn: 'linked',
            });

            $('#simple-date3 .input-group.date').datepicker({
                startView: 2,
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true,
                todayBtn: 'linked',
            });

            $('#simple-date4 .input-daterange').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true,
                todayBtn: 'linked',
            });

            // TouchSpin

            $('#touchSpin1').TouchSpin({
                min: 0,
                max: 100,
                boostat: 5,
                maxboostedstep: 10,
                initval: 0
            });

            $('#touchSpin2').TouchSpin({
                min:0,
                max: 100,
                decimals: 2,
                step: 0.1,
                postfix: '%',
                initval: 0,
                boostat: 5,
                maxboostedstep: 10
            });

            $('#touchSpin3').TouchSpin({
                min: 0,
                max: 100,
                initval: 0,
                boostat: 5,
                maxboostedstep: 10,
                verticalbuttons: true,
            });

            $('#clockPicker1').clockpicker({
                donetext: 'Done'
            });

            $('#clockPicker2').clockpicker({
                autoclose: true
            });

            let input = $('#clockPicker3').clockpicker({
                autoclose: true,
                'default': 'now',
                placement: 'top',
                align: 'left',
            });

            $('#check-minutes').click(function(e){
                e.stopPropagation();
                input.clockpicker('show').clockpicker('toggleView', 'minutes');
            });

        });
    </script>
@endpush
