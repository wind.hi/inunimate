@extends('admin.layouts.master')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active" aria-current="page">DataTables</li>
            </ol>
        </div>

        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Danh sách thuộc tính</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <table class="table align-items-center table-flush" id="dataTable">
                            <thead class="thead-light">
                            <tr>
                                <th>STT</th>
                                <th>Tên thuộc tính</th>
                                <th>Kiểu</th>
                                <th>Danh mục</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>STT</th>
                                <th>Tên thuộc tính</th>
                                <th>Kiểu</th>
                                <th>Danh mục</th>
                                <th>Thao tác</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($attributes as $key=>$attribute)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td id="attribute{{$attribute->id}}">{{$attribute->name}}</td>
                                    <td>{{$attribute->type}}</td>
                                    <?php
                                    $categories = $attribute->categories;
                                    $arrCates = [];
                                    foreach ($categories as $category) {
                                        $arrCates[] = $category->name;
                                    }
                                    $stringCate = implode(', ', $arrCates);
                                    ?>
                                    <td>{{$stringCate}}</td>
                                    <td><a href="{{route('admin.attributes.show', $attribute->id)}}" target="_blank"
                                           class="btn btn-sm btn-primary">chi tiết</a>
                                        <a class="btn btn-sm btn-danger" onclick="return showModalDelete(this);"
                                           data-att_id="{{$attribute->id}}">xóa</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!--Row-->

        <!-- Documentation Link -->
        <div class="row">
            <div class="col-lg-12">
                <p>DataTables is a third party plugin that is used to generate the demo table below. For more
                    information
                    about DataTables, please visit the official <a href="https://datatables.net/" target="_blank">DataTables
                        documentation.</a></p>
            </div>
        </div>

        <!-- Modal Logout -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to logout?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                        <a href="login.html" class="btn btn-primary">Logout</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!---Container Fluid-->

    <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Xóa thuộc tính</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="content-modal"></p>
                    <input type="hidden" name="attribute_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="return deleteAttribute(this);">Đồng ý
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('admin/toast/jquery.toast.min.js')}}" charset="utf-8"></script>

    <!-- Page level custom scripts -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable(); // ID From dataTable
            $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        });

        function showModalDelete(e) {
            let attributeId = $(e).data('att_id');
            let attributeName = $(e).closest($('table#dataTable')).find($('tbody')).find($('td#attribute' + attributeId)).text();

            $("input[name='attribute_id']").val(attributeId);

            $('p#content-modal').empty();
            $('p#content-modal').text('Đồng ý xóa thuộc tính ' + attributeName + ', chỉ được xóa khi thuộc tính này chưa được gán vào sản phẩm nào !');
            $('#exampleModalLong').modal('show');
        }

        function deleteAttribute(e) {
            let attributeId = $("input[name='attribute_id']").val();
            let url = "{{route('admin.attributes.delete', ["id"=>':id'])}}";

            let urlReplace = url.replace(':id', +attributeId);

            $.ajax({
                    url: urlReplace,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}'
                    },
                    success: function (data) {
                        let check = data.check;
                        console.log(check);
                        if (check == '1') {
                            $.toast({
                                position: 'top-right',
                                text: 'Không thể xóa thuộc tính đang có sản phẩm được gán!',
                                icon: 'error',
                                // afterHidden: function () {
                                //     location.reload();
                                // }
                            });
                        } else {
                            $.toast({
                                position: 'top-right',
                                text: 'Đã xóa thuộc tính',
                                icon: 'success',
                                afterHidden: function () {
                                    location.reload();
                                }
                            });
                            $('#exampleModalLong').modal('hide');
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                },
            );
        }
    </script>
@endpush
