<?php

namespace Modules\Attribute\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Attribute API Request
 *
 * Class AttributeCreateApiRequest
 * @package Modules\Api\Http\Requests
 *
 * @property $name
 * @property $type
 * @property $category
 */
class AttributeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
