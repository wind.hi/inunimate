<?php

namespace Modules\Attribute\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Http\Requests\AttributeCreateRequest;
use Modules\Attribute\Http\Requests\AttributeUpdateRequest;
use Modules\Attribute\Repositories\AttributeRepository;
use Modules\Attribute\Service\AttributeService;
use Modules\Category\Entities\Category;

class AttributeController extends Controller
{
    protected $repository;
    protected $attributeService;

    public function __construct(AttributeRepository $repository, AttributeService $attributeService)
    {
        $this->repository = $repository;
        $this->attributeService = $attributeService;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $attributes = $this->repository->with('categories')->orderBy('created_at', 'desc')->get();
        return view('attribute::index', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = Category::query()->where('parent', 0)->get();
        return view('attribute::create', compact('categories'));
    }

    /**
     * @param AttributeCreateRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(AttributeCreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['name', 'type']);

            $this->attributeService->create($data, $request->category);
            DB::commit();
            return redirect()->back()->with('messageSuccess', 'Thêm mới thuộc tính thành công!');
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('attribute::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $attribute = $this->repository->find($id);

        return view('attribute::detail', compact('attribute'));
    }

    /**
     * @param AttributeUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(AttributeUpdateRequest $request, $id)
    {
        try {
            $data = $request->only('name');

            $attribute = $this->repository->update($data, $id);

            return redirect()->back()->with('messageSuccess', 'Cập nhật thuộc tính thành công!');
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            /** @var Attribute $attribute */
            $attribute = $this->repository->find($id);

            $products = $attribute->products;

            if (count($products) > 0) {
                /** = 1 thuộc tính đang có sản phẩm, ko đc xóa */
                $check = 1;
            } else {
                /** có thể xóa */
                $check = 2;
                $attribute->delete();
            }
            return response()->json(['check' => $check], 200);
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }
}
