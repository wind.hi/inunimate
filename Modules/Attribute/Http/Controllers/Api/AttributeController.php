<?php

namespace Modules\Attribute\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Attribute\Http\Requests\AttributeCreateApiRequest;
use Modules\Attribute\Http\Requests\AttributeUpdateApiRequest;
use Modules\Attribute\Repositories\AttributeRepository;
use Modules\Attribute\Transformers\AttributeTransformer;


/**
 * Class AttributeController
 * @property AttributeRepository $repository
 * @package Modules\Attribute\Http\Controllers\Api
 */
class AttributeController extends BaseApiController
{
    /**
     * AttributeController constructor.
     * @param AttributeRepository $repository
     */
    public function __construct(AttributeRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index(Request $request)
    {
        $data = $this->repository->paginate(intval($request->get('per_page')));
        return $this->responseSuccess($this->transform($data, AttributeTransformer::class, $request));
    }

    public function show($identifier, Request $request)
    {
        $entity = $this->repository->find($identifier);
        return $this->responseSuccess($this->transform($entity, AttributeTransformer::class, $request));
    }

    public function store(AttributeCreateApiRequest $request)
    {
        DB::beginTransaction();
        try {
            $storeValues = $request->only([]);
            $entity = $this->repository->create($storeValues);
            DB::commit();
            return $this->responseSuccess($this->transform($entity, AttributeTransformer::class, $request));
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->responseErrors(500, $e->getMessage());
        }
    }

    
    public function update($identifier, AttributeUpdateApiRequest $request)
    {
        $entity = $this->repository->find($identifier);
        $storeValues = $request->only([]);
        DB::beginTransaction();
        try {
            $entity = $this->repository->update($storeValues, $entity->id);
            DB::commit();
            return $this->responseSuccess($this->transform($entity, AttributeTransformer::class, $request));
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->responseErrors(500, $e->getMessage());
        }
    }
}
