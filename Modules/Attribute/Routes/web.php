<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

    Route::prefix('attributes')->group(function() {
        Route::get('', 'AttributeController@index')->name('admin.attributes.index');
        Route::get('/create', 'AttributeController@create')->name('admin.attributes.create');
        Route::post('', 'AttributeController@store')->name('admin.attributes.store');
        Route::get('{id}', 'AttributeController@edit')->name('admin.attributes.show');
        Route::put('{id}', 'AttributeController@update')->name('admin.attributes.update');
        Route::delete('{id}', 'AttributeController@delete')->name('admin.attributes.delete');
    });

});

