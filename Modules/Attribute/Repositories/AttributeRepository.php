<?php

namespace Modules\Attribute\Repositories;

use Modules\Attribute\Entities\Attribute;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class AttributeRepository
 * @package Modules\Platform\User\Repositories
 */
class AttributeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Attribute::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return Attribute
     */
    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return Attribute
     */
    public function update(array $attributes, $id)
    {
        return parent::update($attributes, $id);
    }
}