<?php

namespace Modules\Attribute\Entities;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;
use Modules\Product\Entities\Product;

/**
 * Class Attribute
 * @package Modules\Attribute\Entities
 *
 * @property int $id
 * @property string $name
 * @property string $type
 *
 * Realtions
 * @property-read Collection|Category[] $categories
 * @property-read Collection|Product[] $products
 */
class Attribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Category[]
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'attribute_categories', 'attribute_id', 'category_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Product[]
     */
    public function products(){
        return $this->belongsToMany(Product::class, 'product_attribute_values', 'attribute_id', 'product_id');
    }
}
