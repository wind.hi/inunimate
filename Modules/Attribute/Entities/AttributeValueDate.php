<?php

namespace Modules\Attribute\Entities;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;

/**
 * Class AttributeValueFloat
 * @package Modules\Attribute\Entities
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $product_id
 * @property string $value
 *
 * Realtions
 */
class AttributeValueDate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'attribute_id', 'product_id', 'value'];

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'attribute_value_date';

}
