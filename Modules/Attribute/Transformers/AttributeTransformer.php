<?php
namespace Modules\Attribute\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Attribute\Entities\Attribute;

class AttributeTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Attribute $attribute)
    {
        return [
            'id' => $attribute->id,
            'created_at' => $attribute->created_at,
            'updated_at' => $attribute->updated_at,
        ];
    }
}