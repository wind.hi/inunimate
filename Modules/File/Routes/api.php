<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'file'], function () {
        Route::get('', 'FileController@index');
        Route::get('{id}', 'FileController@show');
        Route::post('', 'FileController@store');
        Route::put('{id}', 'FileController@update');
    });
});

