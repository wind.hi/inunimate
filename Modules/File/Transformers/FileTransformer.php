<?php
namespace Modules\File\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\File\Entities\File;

class FileTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(File $file)
    {
        return [
            'id' => $file->id,
            'created_at' => $file->created_at,
            'updated_at' => $file->updated_at,
        ];
    }
}