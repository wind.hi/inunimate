<?php

namespace Modules\File\Http\Requests;
use App\Http\Requests\ApiBaseRequest;

/**
 * File API Request
 *
 * Class FileCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class FileCreateApiRequest extends ApiBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
