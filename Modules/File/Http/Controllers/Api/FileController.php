<?php

namespace Modules\File\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\File\Http\Requests\FileCreateApiRequest;
use Modules\File\Http\Requests\FileUpdateApiRequest;
use Modules\File\Repositories\FileRepository;
use Modules\File\Transformers\FileTransformer;


/**
 * Class FileController
 * @property FileRepository $repository
 * @package Modules\File\Http\Controllers\Api
 */
class FileController extends BaseApiController
{
    /**
     * FileController constructor.
     * @param FileRepository $repository
     */
    public function __construct(FileRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index(Request $request)
    {
        $data = $this->repository->paginate(intval($request->get('per_page')));
        return $this->responseSuccess($this->transform($data, FileTransformer::class, $request));
    }

    public function show($identifier, Request $request)
    {
        $entity = $this->repository->find($identifier);
        return $this->responseSuccess($this->transform($entity, FileTransformer::class, $request));
    }

    public function store(FileCreateApiRequest $request)
    {
        DB::beginTransaction();
        try {
            $storeValues = $request->only([]);
            $entity = $this->repository->create($storeValues);
            DB::commit();
            return $this->responseSuccess($this->transform($entity, FileTransformer::class, $request));
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->responseErrors(500, $e->getMessage());
        }
    }

    
    public function update($identifier, FileUpdateApiRequest $request)
    {
        $entity = $this->repository->find($identifier);
        $storeValues = $request->only([]);
        DB::beginTransaction();
        try {
            $entity = $this->repository->update($storeValues, $entity->id);
            DB::commit();
            return $this->responseSuccess($this->transform($entity, FileTransformer::class, $request));
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return $this->responseErrors(500, $e->getMessage());
        }
    }
}
