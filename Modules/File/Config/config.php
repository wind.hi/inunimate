<?php

return [
    'name' => 'File',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'file.browse',
        'file.create',
        'file.update',
        'file.destroy'
    ]
];