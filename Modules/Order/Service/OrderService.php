<?php

namespace Modules\Order\Service;

use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;
use Modules\Order\Notifications\CustomerOrderNotification;
use Modules\Order\Notifications\SendNotifiOrderToAdmin;
use Modules\Order\Repositories\OrderRepository;
use Modules\Guest\Repositories\GuestRepository;
use Modules\User\Entities\User;

class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $data
     * @return \Modules\Order\Entities\Order
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create($data)
    {
        $number = mt_rand(1000000, 9999999);

        $data['code'] = 'TMOD' . $number . Order::query()->max('id');

        $order = $this->orderRepository->create($data);

        $customerData = [
            'name' => $data['customer_name'],
            'email' => @$data['customer_email'] ? $data['customer_email'] : null,
            'phone' => $data['customer_phone'],
        ];
        $customer = $this->createCustomer($customerData);

        $admins = User::all();

        if ($customer->email !== null) {
            Notification::send($customer, new CustomerOrderNotification($order, $customerData));
        }

        Notification::send($admins, new SendNotifiOrderToAdmin($order, $customerData));

        return $order;
    }

    /**
     * @param $orderId
     * @param $status
     * @return \Modules\Guest\Entities\Guest
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateStatus($orderId, $status)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($orderId);

        $statusCurrent = $order->status;

        if ($status == '1') {
            if ($statusCurrent == '2' || $statusCurrent == '4') {
                $order->product()->increment('total', $order->quantity);
            }
            $order->status = 1;

        } elseif ($status == '2') {
            if ($statusCurrent == '1') {
                $order->product()->decrement('total', $order->quantity);
            }
            $order->status = 2;

        } elseif ($status == '3') {
            $order->status = 3;

        } elseif ($status == '4') {
            if ($statusCurrent == '1') {
                $order->product()->decrement('total', $order->quantity);
            }
            $order->status = 4;
        }

        $order->save();

    }

    /**
     * @param $customerData
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private function createCustomer($customerData)
    {
        if ($customerData['email'] !== null) {
            if (!Customer::query()->where('email', $customerData['email'])->exists()) {
                $customer = Customer::query()->create($customerData);
            } else {
                $customer = Customer::query()->where('email', $customerData['email'])->first();
            }
        } else {
            $customer = Customer::query()->create($customerData);
        }
        return $customer;
    }
}
