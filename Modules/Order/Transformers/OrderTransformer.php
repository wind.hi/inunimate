<?php
namespace Modules\Order\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Order\Entities\Order;

class OrderTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'created_at' => $order->created_at,
            'updated_at' => $order->updated_at,
        ];
    }
}