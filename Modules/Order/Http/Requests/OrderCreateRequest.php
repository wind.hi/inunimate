<?php

namespace Modules\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Order Request
 *
 * Class OrderCreateRequest
 * @package Modules\Api\Http\Requests
 *
 */
class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'customer_name'         =>  'required',
            'customer_email'        =>  'nullable|email',
            'customer_phone'        =>  'required|regex:/(0)[0-9]{9}/',
            'customer_address'      =>  'required',
            'quantity'     =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'customer_name.required'        => 'Không được để trống',
            'customer_email.email'          => 'Không đúng định dạng',
            'customer_phone.required'       => 'Không được để trống',
            'customer_phone.regex'          => 'Không đúng định dạng',
            'customer_address.required'          => 'Không được để trống',
            'quantity.required'     => 'Không được để trống',
        ];
    }
}
