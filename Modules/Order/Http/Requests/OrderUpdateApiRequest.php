<?php

namespace Modules\Order\Http\Requests;
use App\Http\Requests\ApiBaseRequest;

/**
 * Order API Request
 *
 * Class OrderCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class OrderUpdateApiRequest extends ApiBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
