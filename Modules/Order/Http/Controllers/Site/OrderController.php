<?php

namespace Modules\Order\Http\Controllers\Site;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;
use Modules\Guest\Entities\Guest;
use Modules\Order\Http\Requests\OrderCreateRequest;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Service\OrderService;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    protected $repository;
    protected $orderService;

    public function __construct(OrderRepository $repository, OrderService $orderService)
    {
        $this->repository = $repository;

        $this->orderService = $orderService;
    }

    public function index()
    {
        return view('order::index');
    }

    public function create()
    {
        return view('order::create');
    }

    /**
     * @param OrderCreateRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(OrderCreateRequest $request)
    {
        DB::beginTransaction();
        try {

            $data = $request->only('product_id', 'customer_name', 'customer_email', 'customer_phone', 'customer_address','note', 'quantity');


            $order = $this->orderService->create($data);

            DB::commit();
            return redirect()->route('orders.success');
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('order::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('order::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function orderSuccess(){
        return view('product::site.thanks_you_order');
    }
}
