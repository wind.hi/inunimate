<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;
use Modules\Guest\Entities\Guest;
use Modules\Order\Http\Requests\OrderCreateRequest;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Service\OrderService;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    protected $repository;
    protected $orderService;

    public function __construct(OrderRepository $repository, OrderService $orderService)
    {
        $this->repository = $repository;

        $this->orderService = $orderService;
    }

    public function index()
    {
        $orders = $this->repository->orderBy('created_at', 'desc')->get();
        return view('order::index', compact('orders'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request){
        DB::beginTransaction();
        try {
            $orderId = $request->input('order_id');
            $status = $request->input('status');

            $order = $this->orderService->updateStatus($orderId, $status);

            DB::commit();
            return redirect()->back()->with('messageSuccess', 'Đặt lịch thành công. Thanks ciu so muck!');
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $order = $this->repository->find($id);

        return view('order::detail', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('order::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
