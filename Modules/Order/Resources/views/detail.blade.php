@extends('admin.layouts.master')
@section('content')
        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
            <div class="row">
                <div class="col-lg-1">

                </div>

                <div class="col-lg-10">
                    <!-- Horizontal Form -->
                    <div class="row">
                         <div class="col-lg-6">
                             <div class="card mb-4">
                                 <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                     <h6 class="m-0 font-weight-bold text-primary">Thông tin đơn hàng</h6>
                                 </div>
                                 <div class="card-body">
                                     <fieldset class="form-group">
                                         <div class="row">

                                             <legend class="col-form-label col-sm-3 pt-0">Mã Order</legend>
                                             <div class="col-sm-9">
                                                 <div class="">
                                                     <label class="">{{$order->code}}</label>
                                                 </div>
                                             </div>

                                             <legend class="col-form-label col-sm-3 pt-0">Ngày đặt</legend>
                                             <div class="col-sm-9">
                                                 <div class="">
                                                     <label class="">{{\Carbon\Carbon::parse($order->created_at)->format('d-m-Y H:i')}}</label>
                                                 </div>
                                             </div>

                                             <legend class="col-form-label col-sm-3 pt-0">Sản phẩm</legend>
                                             <div class="col-sm-9">
                                                 <div class="">
                                                     <label class="">{{$order->product->name}}</label>
                                                 </div>
                                             </div>

                                             <legend class="col-form-label col-sm-3 pt-0">Số lượng</legend>
                                             <div class="col-sm-9">
                                                 <div class="">
                                                     <label class="">{{$order->quantity}} ({{$order->product->unit}})</label>
                                                 </div>
                                             </div>
                                             <?php
                                             if($order->status == \Modules\Order\Entities\Order::NEW){
                                                 $status = 'mới';
                                             }elseif($order->status == \Modules\Order\Entities\Order::CONFIRMED){
                                                 $status = 'đã xác nhận';
                                             }elseif($order->status == \Modules\Order\Entities\Order::CLOSE){
                                                 $status = 'hủy';
                                             }else{
                                                 $status = 'hoàn thành';
                                             }
                                             ?>
                                             <legend class="col-form-label col-sm-3 pt-0">Trạng thái</legend>
                                             <div class="col-sm-9">
                                                 <div class="btn-group mb-1">
                                                     <button type="button" class="btn btn-primary dropdown-toggle btn-status" data-toggle="dropdown"
                                                             aria-haspopup="true" aria-expanded="false">
                                                         {{$status}}
                                                     </button>
                                                     <div class="dropdown-menu">
                                                         <a class="dropdown-item" data-order_id = "{{$order->id}}" data-status = "1" onclick="return updateStatusOrder(this);">mới</a>
                                                         <a class="dropdown-item" data-order_id = "{{$order->id}}" data-status = "2" onclick="return updateStatusOrder(this);">đã xác nhận</a>
                                                         <a class="dropdown-item" data-order_id = "{{$order->id}}" data-status = "3" onclick="return updateStatusOrder(this);">hủy</a>
                                                         <a class="dropdown-item" data-order_id = "{{$order->id}}" data-status = "4" onclick="return updateStatusOrder(this);">hoàn thành</a>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </fieldset>

                                 </div>
                             </div>
                         </div>

                        <div class="col-lg-6">
                            <div class="card mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Thông tin KH</h6>
                                </div>
                                <div class="card-body">
                                    <fieldset class="form-group">
                                        <div class="row">

                                            <legend class="col-form-label col-sm-3 pt-0">Họ tên</legend>
                                            <div class="col-sm-9">
                                                <div class="">
                                                    <label class="">{{$order->customer_name}}</label>
                                                </div>
                                            </div>

                                            <legend class="col-form-label col-sm-3 pt-0">SĐT</legend>
                                            <div class="col-sm-9">
                                                <div class="">
                                                    <label class="">{{$order->customer_phone}}</label>
                                                </div>
                                            </div>

                                            <legend class="col-form-label col-sm-3 pt-0">Email</legend>
                                            <div class="col-sm-9">
                                                <div class="">
                                                    <label class="">{{$order->customer_email}}</label>
                                                </div>
                                            </div>

                                            <legend class="col-form-label col-sm-3 pt-0">Địa chỉ</legend>
                                            <div class="col-sm-9">
                                                <div class="">
                                                    <label class="">{{$order->customer_address}}</label>
                                                </div>
                                            </div>

                                            <legend class="col-form-label col-sm-3 pt-0">Ghi chú</legend>
                                            <div class="col-sm-9">
                                                <div class="">
                                                    <label class="">{{$order->note}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-1">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Sản phẩm</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>Mã sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Danh mục</th>
                                    <th>Số lượng đặt</th>
                                    <th>Số lượng trong kho</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <?php
                                        foreach ($order->product->category as $category){
                                            $arrCategory[] = $category->name;
                                        }

                                        $stringCategory = implode(',', $arrCategory);
                                    ?>

                                    <td>{{$order->product->code}}</td>
                                    <td>{{$order->product->name}}</td>
                                    <td>{{$stringCategory}}</td>
                                    <td>{{$order->quantity}}</td>
                                    <td>{{$order->product->total}}</td>
                                    <td><a href="{{route('admin.products.show', $order->product->id)}}" target="_blank" class="btn btn-sm btn-primary">chi tiết sản phẩm</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>

        </div>
        <!---Container Fluid-->
@endsection

@push('scripts')
    <script>

        function updateStatusOrder(e){
            if(confirm('Đồng ý thay đổi trạng thái đơn hàng ?')) {
                let orderId = $(e).data('order_id');
                let status = $(e).data('status');
                $.ajax({
                        url: '{{route('ajax.orders.update.status')}}',
                        method: 'POST',
                        data: {
                            _token: '{{csrf_token()}}',
                            order_id: orderId,
                            status: status
                        },
                        success: function (data) {
                            location.reload();
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    },
                );
            }

        }

    </script>
@endpush
