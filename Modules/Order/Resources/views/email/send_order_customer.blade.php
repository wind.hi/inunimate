
<div style="margin:0;padding:0;background-color:#f3f3f3" bgcolor="#f3f3f3">
    <div style="background:#f3f3f3;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;min-height:100%!important;line-height:1.5em;margin:0;padding:0;width:100%!important" bgcolor="#f3f3f3">
        <table border="0" cellpadding="0" cellspacing="0" width="96%" style="margin-left:2%">
            <tbody>
            <tr>
                <td align="center" bgcolor="#f3f3f3" valign="top" style="color:#000000;">
                    <h2 style="margin:0;">
                        <img src="https://uphinh.vn/images/2021/02/21/34c6cba099c671e24a2ace8a9f33da57.jpg" width="160" alt="" style="border:0" class="CToWUd">
                    </h2>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;color:black">
                        <tbody>
                        <tr>
                            <td style="padding:20px;border-bottom:2px solid #dddddd" bgcolor="#ffffff">
                                <h3 style="color: #000000">Thân gửi: {{$customerName}} !</h3>
                                <p style="text-align:justify;color: #000000">Chúc mừng quý khách đã đặt hàng thành công. Cảm ơn quý khách đã tin tưởng và sử dụng sản phẩm của công ty.
                                </p>
                                <p style="color: #000000">
                                    <br>
                                    <span style="font-size: 14px; font-weight: 400;color:#000000; "> Sản phẩm: <strong>{{$productName}}</strong></span> <br/>
                                    <span style="font-size: 14px; font-weight: 400;color:#000000; "> Số lượng đặt: <strong>{{$quantity}}</strong> ({{$productUnit}})</span>
                                </p>
                                </br>
                                <span style="color: #000000">Ngày tạo đơn: {{$createdAt}} </span>
                                <p style="color: #000000">Nếu có bất kì thắc mắc nào, vui lòng liên hệ <a href="tienminhsupport@gmail.com" title="" style="color:#2baadf;text-decoration:underline" target="_blank">tienminhsupport@gmail.com</a> để được hỗ trợ.</p>
                                <p style="color: #000000">Trân trọng.</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <img width="1px" height="1px" alt="" src="https://ci4.googleusercontent.com/proxy/Kw7lRRiT4uBXjab90gmUf2fH7UyEaGpUF-lCh8qw7qas1fFiPaQ8DAOEWiTjgi7yjfFlDOferxzIJwI0qvQ4f24eRkypjPWkTH-oi79mcZAR5XXyxWb3ATDAR6Ld5MYJlRAG2ecP9_hbpyh7r28TJOTigQZV8cJePhSJ5e5mGe4PtPpghsDdOMGA_RTAMZadvnEya-hBwePnLcLT94PRLJ8yDxPhKP2YDVAozZnuDDPZStv8a-jAtex9tB0viWvh0_fDBmAdROxMb56qHvZxNyrmDM_Jxk4S=s0-d-e1-ft#http://email.topcv.vn/o/eJwNzEsOgyAQANDTlCWZgZEJCzaN9hoNP9GkokFq09u37wAvOU4IMYrVKVAASmnQAxJLlMAM9ACaRhjvlulG0PcjXvKqYnGQgifLaDLYqEzmMAcd0aCxc8h6EM31xddSy_uba706EsK_KJtfXzLum-iu5TP35-HP87O39ANkayn3"
         class="CToWUd">
</div>
