@extends('admin.layouts.master')
@section('content')
        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Đơn hàng</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="./">Home</a></li>
                    <li class="breadcrumb-item">Tables</li>
                    <li class="breadcrumb-item active" aria-current="page">DataTables</li>
                </ol>
            </div>

            <!-- Row -->
            <div class="row">
                <!-- Datatables -->
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        </div>
                        <div class="table-responsive p-3">
                            <table class="table align-items-center table-flush" id="dataTable">
                                <thead class="thead-light">
                                <tr>
                                    <th>Mã Order</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Họ tên KH</th>
                                    <th>SĐT</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Mã Order</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Họ tên KH</th>
                                    <th>SĐT</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{$order->code}}</td>
                                        <td>{{$order->product->name}}</td>
                                        <td>{{$order->quantity}} ({{$order->product->unit}})</td>
                                        <td>{{$order->customer_name}}</td>
                                        <td>{{$order->customer_phone}}</td>
                                               <?php
                                                    if($order->status == \Modules\Order\Entities\Order::NEW){
                                                        $status = 'mới';
                                                    }elseif($order->status == \Modules\Order\Entities\Order::CONFIRMED){
                                                        $status = 'đã xác nhận';
                                                    }elseif($order->status == \Modules\Order\Entities\Order::CLOSE){
                                                        $status = 'hủy';
                                                    }else{
                                                        $status = 'hoàn thành';
                                                    }
                                                ?>
                                        <td>{{$status}}</td>
                                        <td>
                                            <a href="{{route('admin.orders.show', $order->id)}}" class="btn btn-info btn-sm" title="chi tiết">
                                                <i class="fas fa-info-circle"></i>
                                            </a>

                                            <a href="#" class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!--Row-->

            <!-- Documentation Link -->
            <div class="row">
                <div class="col-lg-12">
                    <p>DataTables is a third party plugin that is used to generate the demo table below. For more information
                        about DataTables, please visit the official <a href="https://datatables.net/" target="_blank">DataTables
                            documentation.</a></p>
                </div>
            </div>

            <!-- Modal Logout -->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to logout?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                            <a href="login.html" class="btn btn-primary">Logout</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!---Container Fluid-->
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable(); // ID From dataTable
            $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        });
    </script>
@endpush
