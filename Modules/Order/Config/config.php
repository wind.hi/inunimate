<?php

return [
    'name' => 'Order',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'order.browse',
        'order.create',
        'order.update',
        'order.destroy'
    ]
];