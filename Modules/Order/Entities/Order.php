<?php

namespace Modules\Order\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

/**
 * Class Order
 * @property int $id
 * @package Modules\Order\Entities
 *
 * Relations
 * @property-read Product $product
 */
class Order extends Model
{
    const NEW = 1;
    const CONFIRMED = 2;
    const CLOSE = 3;
    const SUCCESS = 4;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'product_id', 'quantity', 'customer_name', 'customer_phone', 'customer_email', 'customer_address', 'status', 'note', 'code'];

    protected $dates = ['created_at', 'updated_at'];

    protected $table = 'orders';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Product
     */
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
