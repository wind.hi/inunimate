<?php

namespace Modules\Order\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\Order\Entities\Order;

class CustomerOrderNotification extends Notification
{
    use Queueable;
    private $order;
    private $customerData;

    /**
     * CustomerOrderNotification constructor.
     * @param Order $order
     * @param array $customerData
     */
    public function __construct($order, $customerData)
    {
        $this->order = $order;
        $this->customerData = $customerData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', '', ''];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $product = $this->order->product;
        return (new MailMessage)->view(
            'order::email.send_order_customer', [
                'customerName' => $this->customerData['name'],
                'productName' => $product->name,
                'productUnit' => $product->unit,
                'quantity' => $this->order->quantity,
                'orderCode' => $this->order->code,
                'createdAt' => $this->order->created_at->format('d/m/Y H:i'),
            ]
        )
            ->subject("Thông báo đặt hàng thành công !");
    }

}
