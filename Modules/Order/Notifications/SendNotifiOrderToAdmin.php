<?php

namespace Modules\Order\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\Order\Entities\Order;

class SendNotifiOrderToAdmin extends Notification
{
    use Queueable;
    private $order;
    private $customerData;

    /**
     * SendNotifiOrderToAdmin constructor.
     * @param Order $order
     * @param array $customerData
     */
    public function __construct($order, $customerData)
    {
        $this->order = $order;
        $this->customerData = $customerData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', ''];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $product = $this->order->product;
        return (new MailMessage)->view(
            'order::email.send_notifi_order_to_admin', [
                'customerName' => $this->customerData['name'],
                'productName' => $product->name,
                'productUnit' => $product->unit,
                'quantity' => $this->order->quantity,
                'orderCode' => $this->order->code,
                'createdAt' => $this->order->created_at->format('d/m/Y H:i'),
            ]
        )
            ->subject("Thông báo có đơn hàng mới");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'time' => $this->order->created_at->format('d/m/Y H:i'),
            'title' => "Khách hàng ".$this->customerData['name']. " vừa đặt 1 đơn hàng.",
            'content' => "Mã đơn hàng ".$this->order->code
        ];
    }
}
