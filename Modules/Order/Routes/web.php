<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

    Route::prefix('orders')->group(function() {
        Route::get('', 'OrderController@index')->name('admin.orders.index');

        Route::get('{id}', 'OrderController@show')->name('admin.orders.show');

        /** ajax handle */
        Route::post('update-status', 'OrderController@updateStatus')->name('ajax.orders.update.status');

    });

});

Route::group(['namespace' => 'Site'], function (){
    Route::prefix('orders')->group(function() {
        Route::get('/', 'OrderController@index');

        Route::post('', 'OrderController@store')->name('orders.store');
        Route::get('/order-success', 'OrderController@orderSuccess')->name('orders.success');

    });
});
