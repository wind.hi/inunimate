<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'order'], function () {
        Route::get('', 'OrderController@index');
        Route::get('{id}', 'OrderController@show');
        Route::post('', 'OrderController@store');
        Route::put('{id}', 'OrderController@update');
    });
});

