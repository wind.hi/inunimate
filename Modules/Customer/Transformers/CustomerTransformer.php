<?php
namespace Modules\Customer\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Customer\Entities\Customer;

class CustomerTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Customer $customer)
    {
        return [
            'id' => $customer->id,
            'created_at' => $customer->created_at,
            'updated_at' => $customer->updated_at,
        ];
    }
}