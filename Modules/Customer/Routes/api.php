<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'customer'], function () {
        Route::get('', 'CustomerController@index');
        Route::get('{id}', 'CustomerController@show');
        Route::post('', 'CustomerController@store');
        Route::put('{id}', 'CustomerController@update');
    });
});

