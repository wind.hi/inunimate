<?php

namespace Modules\Customer\Http\Requests;
use App\Http\Requests\ApiBaseRequest;

/**
 * Customer API Request
 *
 * Class CustomerCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class CustomerCreateApiRequest extends ApiBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
