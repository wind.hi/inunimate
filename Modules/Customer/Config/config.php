<?php

return [
    'name' => 'Customer',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'customer.browse',
        'customer.create',
        'customer.update',
        'customer.destroy'
    ]
];