<?php

namespace Modules\Guest\Repositories;

use Modules\Guest\Entities\Guest;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class GuestRepository
 * @package Modules\Platform\User\Repositories
 */
class GuestRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Guest::class;
    }

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return Guest
     */
    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return Guest
     */
    public function update(array $attributes, $id)
    {
        return parent::update($attributes, $id);
    }
}