<?php
namespace Modules\Guest\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Guest\Entities\Guest;

class GuestTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    public function transform(Guest $guest)
    {
        return [
            'id' => $guest->id,
            'created_at' => $guest->created_at,
            'updated_at' => $guest->updated_at,
        ];
    }
}