<?php

namespace Modules\Guest\Http\Requests;
use App\Http\Requests\ApiBaseRequest;

/**
 * Guest API Request
 *
 * Class GuestCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class GuestUpdateApiRequest extends ApiBaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
