<?php

return [
    'name' => 'Guest',
    /**
     * Always use lower name without custom characters, spaces, etc
     */
    'permissions' => [
        'guest.browse',
        'guest.create',
        'guest.update',
        'guest.destroy'
    ]
];