<?php

namespace Modules\Guest\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Guest
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @package Modules\Guest\Entities
 */
class Guest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'first_name', 'last_name', 'email', 'phone', 'password'
    ];

    protected $dates = ['created_at', 'updated_at'];

}
