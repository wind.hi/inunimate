<?php

namespace Modules\User\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * User API Request
 *
 * Class UpdatePasswordRequest
 * @package Modules\Api\Http\Requests
 */
class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'current_password' => 'required',
            'new_password' => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required|min:6',
        ];
    }
}
