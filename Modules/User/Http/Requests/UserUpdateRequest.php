<?php

namespace Modules\User\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * User API Request
 *
 * Class UserCreateApiRequest
 * @package Modules\Api\Http\Requests
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}
