<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\User\Http\Requests\UpdatePasswordRequest;
use Modules\User\Http\Requests\UserCreateRequest;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Repositories\UserRepository;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $admins = $this->repository->orderBy('created_at', 'desc')->get();
        return view('user::index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param UserCreateRequest $request
     * @return Renderable
     */
    public function store(UserCreateRequest $request)
    {
        try {
            $data = $request->only('email', 'name', 'password');
            $data['password'] = bcrypt($data['password']);

            $user = $this->repository->create($data);

            return redirect()->back()->with('messageSuccess', 'Thêm mới tài khoản thành công!');
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param UserUpdateRequest $request
     * @param int $id
     * @return Renderable
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $data = $request->only('name');

            $admin = $this->repository->update($data, $id);

            return redirect()->back()->with('messageSuccess', 'Cập nhật tài khoản thành công!');
        } catch (\Exception $exception) {
            return response()->json(["errors" => $exception->getMessage()], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function getLogin()
    {
        if (auth()->check()) {
            return redirect(route('admin.index'));
        }
        return view('user::login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function postLogin(Request $request)
    {
        try {
            $email = $request->input('email');
            $password = $request->input('password');
            if (auth()->attempt(['email' => $email, 'password' => $password])) {
                return redirect()->route('admin.index');
            } else {
                return redirect()->back()->with('error', 'Sai tài khoản hoặc mật khẩu !');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logOut()
    {
        auth()->logout();

        return redirect(route('admin.get.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfile(){
        $adminId = auth()->id();
        $admin = $this->repository->find($adminId);

        return view('user::detail', compact('admin'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFormChangePass(){
        return view('user::change_pass');
    }

    /**
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function updatePassword(UpdatePasswordRequest $request){
        try {
            $user = auth()->user();

            if ($user->password && !Hash::check($request->input('current_password'), $user->password)) {
                return redirect()->back()->with('error', 'Mật khẩu hiện tại không đúng !');
            }

            $user->password = Hash::make($request->get('new_password'));
            $user->save();

            return redirect()->back()->with('messageSuccess', 'Thay đổi mật khẩu thành công !');
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}
