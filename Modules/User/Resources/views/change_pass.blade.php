@extends('admin.layouts.master')
@section('content')
    <style>
        label.error{
            color: firebrick;
            font-size: 13px;
        }
    </style>

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Forms</li>
                <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
            </ol>
        </div>
        @if(session()->has('messageSuccess'))
            <div class="alert alert-success">
                {{ session()->get('messageSuccess') }}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-6">
                <!-- Form Basic -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Đổi mật khẩu</h6>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.update.password')}}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhập mật khẩu cũ</label>
                                <input type="password" class="form-control height-control" id=""
                                       name="current_password" value=""
                                >
                            </div>

                            @error('current_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mật khẩu mới</label>
                                <input type="password" class="form-control" id=""
                                       name="new_password" value=""
                                >
                            </div>
                            @error('new_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhập lại mật khẩu</label>
                                <input type="password" class="form-control" id=""
                                       name="password_confirmation" value=""
                                >
                            </div>
                            @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <button type="submit" class="btn btn-primary">lưu</button>
                        </form>

                    </div>
                </div>
            </div>

            <div class="col-lg-2">
            </div>

        </div>

    </div>

@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{asset('site/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <script>
        // $.validator.addMethod("validatePhone", function (value, element) {
        //     return this.optional(element) || /(84|0|\+84)[0-9]{9}$/i.test(value);
        // }, "Vui lòng nhập đúng định dạng điện thoại. vd: 0363676992");
        //
        // $(document).ready(function () {
        //     // $("p#please-wait").css('visibility', 'hidden');
        //     $("#frm-updatePass").validate({
        //         rules: {
        //             old_password: "required",
        //             new_password : {
        //                 required:true,
        //                 minlength : 6
        //             },
        //             re_password : {
        //                 required:true,
        //                 minlength : 6,
        //                 equalTo : "#password"
        //             }
        //         },
        //         messages: {
        //             "new_password": {
        //                 required: "* Vui lòng nhập mật khẩu mới",
        //                 minlength: "* Mật khẩu từ 6 ký tự trở lên",
        //                 equalTo: "* Vui lòng nhập lại mật khẩu",
        //             },
        //         },
        //
        //     });
        // });
    </script>

@endpush
