@extends('admin.layouts.master')
@section('content')
    <style>
        label.error{
            color: firebrick;
            font-size: 13px;
        }
    </style>

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Forms</li>
                <li class="breadcrumb-item active" aria-current="page">Form Basics</li>
            </ol>
        </div>
        @if(session()->has('messageSuccess'))
            <div class="alert alert-success">
                {{ session()->get('messageSuccess') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-6">
                <!-- Form Basic -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Thông tin tài khoản</h6>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('admin.user.update', $admin->id)}}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id=""
                                       name="email" value="{{$admin->email}}" readonly
                                >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên hiển thị</label>
                                <input type="text" class="form-control" id=""
                                       name="name" value="{{$admin->name}}"
                                >
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="exampleInputEmail1">Ngày tạo: </label>
                                <span><strong>{{$admin->created_at->format('d/m/Y H:i')}}</strong></span>
                            </div>
                            <button type="submit" class="btn btn-primary">lưu</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
            </div>

        </div>

    </div>

@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{asset('site/js/jquery.validate.min.js')}}" type="text/javascript"></script>


@endpush
