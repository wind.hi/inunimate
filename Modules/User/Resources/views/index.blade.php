@extends('admin.layouts.master')
@section('content')
    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">

        <div class="row">
            <div class="col-lg-12 mb-4">
                <!-- Simple Tables -->
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Danh sách tài khoản quản trị</h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th>STT</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $key=>$admin)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$admin->email}}</td>
                                    <td>{{$admin->name}}</td>
                                    <td><a href="" target="_blank" class="btn btn-sm btn-primary">gỡ bỏ</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>

    </div>
    <!---Container Fluid-->


@endsection

@push('scripts')
@endpush
