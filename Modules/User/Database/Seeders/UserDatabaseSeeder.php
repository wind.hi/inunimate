<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Model::unguard();
        // $this->call("OthersTableSeeder");
        DB::table('users')->insert(
            [
                [
                    'email' => 'admin@gmail.com',
                    'name' => 'Admin',
                    'password' => bcrypt('123456a@'),
                ],
                [
                    'email' => 'thaohihi@gmail.com',
                    'name' => 'Thao123',
                    'password' => bcrypt('123456a@'),
                ],
            ]
        );
    }
}
