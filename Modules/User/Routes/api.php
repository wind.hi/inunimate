<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'UserController@index');
        Route::get('{id}', 'UserController@show');
        Route::post('', 'UserController@store');
        Route::put('{id}', 'UserController@update');
    });
});

