<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function (){

    Route::group(['prefix' => 'login'], function (){
        Route::get('/', 'UserController@getLogin')->name('admin.get.login');
        Route::post('/post', 'UserController@postLogin')->name('admin.post.login');
    });

    Route::get('/logout', 'UserController@logOut')->name('admin.logout');

    Route::group(['middleware' => 'auth'], function (){
        Route::get('/list', 'UserController@index')->name('admin.list');
        Route::get('/create', 'UserController@create')->name('admin.user.create');
        Route::post('/store', 'UserController@store')->name('admin.user.store');
        Route::put('/update/{id}', 'UserController@update')->name('admin.user.update');
        Route::get('/profile', 'UserController@showProfile')->name('admin.profile');
        Route::get('/change-pass', 'UserController@showFormChangePass')->name('admin.show.change.pass');
        Route::put('/change-pass', 'UserController@updatePassword')->name('admin.update.password');
    });
});
